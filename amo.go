package amocrm

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"path"
	"time"

	"github.com/pkg/errors"
	"golang.org/x/oauth2"
)

const (
	MaxRequestAttempts         = 3
	TimeFormatInSheets         = "2006-01-02 15:04:05"
	ContactsMaxLimitPerRequest = 250
	LeadsMaxLimitPerRequest    = 500
	CacheDuration              = 5 * time.Minute
	debugMode                  = true
	customFieldCode            = "custom_field"
)

var (
	// Hostname — Хостнейм серверов AmoCRM
	Hostname, _ = url.Parse("https://amocrm.ru")
	ApiPrefix   = "/api/v4"
)

type ClientBase struct {
	url        *url.URL
	httpClient *http.Client
	sessionID  string
	cachePath  string

	ClientID     string
	ClientSecret string
	RedirectURI  string
}

type ClientParams struct {
	ClientID     string
	ClientSecret string
	RedirectURI  string
}

func NewClient(params ClientParams) (*ClientBase, error) {
	jar, _ := cookiejar.New(nil)
	c := &ClientBase{
		httpClient: &http.Client{
			Jar:     jar,
			Timeout: 10 * time.Second,
		},
		ClientID:     params.ClientID,
		ClientSecret: params.ClientSecret,
		RedirectURI:  params.RedirectURI,
	}

	return c, nil
}

type ClientSpecific struct {
	// сделать что-либо, если токен был обновлен, опциональный параметр. Request будет неявно обновлять токен
	// ТОЛЬКО ЕСЛИ задан этот колбэк
	OnTokenRefreshed func(*oauth2.Token)

	baseclient *ClientBase
	token      *oauth2.Token
	// потому что апи раскидано примерно по трилионам адресов, и нам нужно четко знать к какому адресу
	// принадлежит пользователь иначе гг
	subdomain string

	// описания кастомных филдов, загружается только 1 раз при инициализации клиента (т.к. не препдолагается
	// создавать по новому филду в секунду), после подтягивается именно отсюда
	fieldsDescription map[string][]*CustomFieldDescription
}

func (c *ClientBase) As(subdomain string, userToken *oauth2.Token) *ClientSpecific {
	return &ClientSpecific{
		baseclient:        c,
		token:             userToken,
		subdomain:         subdomain,
		fieldsDescription: make(map[string][]*CustomFieldDescription),
	}
}

func (c *ClientSpecific) As(subdomain string, usertoken *oauth2.Token) *ClientSpecific {
	return c.baseclient.As(subdomain, usertoken)
}

func (c *ClientSpecific) Request(method, p string, query url.Values, storeIn interface{}, requestBody []byte) error {
	queryStr := ""
	if query != nil {
		queryStr = query.Encode()
	}

	endpoint := &url.URL{
		Host:     c.subdomain + "." + Hostname.Host,
		Scheme:   "https",
		Path:     path.Join(ApiPrefix, p),
		RawQuery: queryStr,
	}
	request, _ := http.NewRequest(method, endpoint.String(), bytes.NewBuffer(requestBody))
	request.Header.Add("Authorization", c.token.Type()+" "+c.token.AccessToken)

	resp, err := c.baseclient.httpClient.Do(request)
	if err != nil {
		return errors.Wrap(err, "requesting")
	}

	if resp.StatusCode == http.StatusUnauthorized {
		if c.OnTokenRefreshed == nil {
			return errors.New("must recreate oauth token explicitly")
		}
		newOne, err := c.baseclient.RefreshToken(c.subdomain, c.token.RefreshToken)
		if err != nil {
			return errors.Wrap(err, "recreating oauth token")
		}

		c.token = newOne
		c.OnTokenRefreshed(newOne)

		return c.Request(method, p, query, storeIn, requestBody)
	}

	if storeIn == nil {
		return nil
	}

	if resp.StatusCode == http.StatusNoContent {
		return &ErrorNoContent{}
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return errors.Wrap(err, "reading data from response")
	}

	if resp.Header.Get("Content-Type") == "application/problem+json" {
		errData := &ErrorResponse{}
		err := json.Unmarshal(body, errData)
		if err != nil {
			return errors.Wrap(err, "can't even decode error response")
		}

		return errData
	}

	err = json.Unmarshal(body, storeIn)
	if err != nil {
		return errors.Wrap(err, "decoding response")
	}

	return nil
}
