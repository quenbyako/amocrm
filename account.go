package amocrm

import (
	"encoding/json"
	"strings"
	"time"
)

type DatesPattern struct {
	Date      string `json:"date"`      //	Дата, формат зависит от выбранного формата в аккаунте
	Time      string `json:"time"`      //	Время, формат зависит от выбранного формата в аккаунте
	DateTrime string `json:"date_time"` //	Дата и время, формат зависит от выбранного формата в аккаунте
	TimeFull  string `json:"time_full"` //	Время с точностью до секунды, формат зависит от выбранного формата в аккаунте
}

type Account struct {
	ID                      int       // Уникальный идентификатор аккаунта
	Name                    string    // Название аккаунта
	Subdomain               string    // Уникальный субдомен данного аккаунта
	CreatedAt               time.Time // Дата создания аккаунта в Unix Timestamp
	CreatedBy               int       // ID пользователя, который создал аккаунт
	UpdatedAt               time.Time // Дата последнего изменения свойства аккаунта в Unix Timestamp
	UpdatedBy               int       // ID пользователя, который последним менял свойства аккаунта
	CurrentUserID           int       // ID текущего пользователя
	Country                 string    // Страна, указанная в настройках аккаунта
	CustomersMode           string    // Режим покупателей. Возможные варианты: unavailable (функционал недоступен), disabled (функцонал отключен), segments (сегментация), dynamic (deprecated), periodicity (периодические покупки)
	IsUnsorted_on           bool      // Включен ли функционал “Неразобранного” в аккаунте
	IsLossReasonEnabled     bool      // Включен ли функционал причин отказа
	IsHelpbotEnabled        bool      // Включен ли функционал Типовых вопросов (доступен только на профессиональном тарифе)
	IsTechnicalAccount      bool      // Является ли данный аккаунт техническим
	ContactNameDisplayOrder int       // Порядок отображения имен контактов (1 – Имя, Фамилия; 2 – Фамилия, Имя)
	AmojoID                 string    // Уникальный идентификатор аккаунта для работы с сервисом чатов amoJo
	Version                 int       // Текущая версия amoCRM
	// _embedded[amojo_rights]     object     Требуется GET параметр with. Объект настроек чатов
	// _embedded[amojo_rights][can_direct]     object     Требуется GET параметр with. Доступны ли внутренние чаты
	// _embedded[amojo_rights][can_create_groups]     object     Требуется GET параметр with. Доступна ли возможность создавать групповые чаты
	// _embedded[users_groups]     array     Требуется GET параметр with. Массив объектов групп пользователей аккаунта
	// _embedded[users_groups][0][id]     int     Требуется GET параметр with. ID группы пользователей
	// _embedded[users_groups][0][name]     string     Требуется GET параметр with. Название группы пользователей
	// _embedded[task_types]     array     Требуется GET параметр with. Типы задач, доступные в аккаунте
	// _embedded[task_types][0][id]     int     Требуется GET параметр with. ID типа задач
	// _embedded[task_types][0][name]     string     Требуется GET параметр with. Название типа задач
	// _embedded[task_types][0][color]     string     Требуется GET параметр with. Цвет типа задач
	// _embedded[task_types][0][icon_id]     int     Требуется GET параметр with. ID иконки типа задач
	// _embedded[task_types][0][code]     string     Требуется GET параметр with. Код типа задач
	// _embedded[datetime_settings]     object     Требуется GET параметр with. Настройки и форматы даты и времени в аккаунте
	// _embedded[entity_names]     object     Требуется GET параметр with. Настройки названия сущностей
	//Currency     currency.Unit                        `json:"currency" unmrashal:"ParseCurrency"` // Валюта аккаунта (используемая при работе с бюджетом сделок). Не связано с биллинговой информацией самого аккаунта.
}

// json representation of account (cause encoding/json can't decode implicit json objects)
type accountJSON struct {
	ID                      int    `json:"id"`
	Name                    string `json:"name"`
	Subdomain               string `json:"subdomain"`
	CreatedAt               int    `json:"created_at"`
	CreatedBy               int    `json:"created_by"`
	UpdatedAt               int    `json:"updated_at"`
	UpdatedBy               int    `json:"updated_by"`
	CurrentUserID           int    `json:"current_user_id"`
	Country                 string `json:"country"`
	CustomersMode           string `json:"customers_mode"`
	IsUnsorted_on           bool   `json:"is_unsorted_on"`
	IsLossReasonEnabled     bool   `json:"is_loss_reason_enabled"`
	IsHelpbotEnabled        bool   `json:"is_helpbot_enabled"`
	IsTechnicalAccount      bool   `json:"is_technical_account"`
	ContactNameDisplayOrder int    `json:"contact_name_display_order"`
	AmojoID                 string `json:"amojo_id"`
	Version                 int    `json:"version"`
}

func (a *Account) UnmarshalJSON(b []byte) error {
	rawMessage := new(accountJSON)

	err := json.Unmarshal(b, rawMessage)
	if err != nil {
		return err
	}

	a.ID = rawMessage.ID
	a.Name = rawMessage.Name
	a.Subdomain = rawMessage.Subdomain
	a.CreatedAt = time.Unix(int64(rawMessage.CreatedAt), 0)
	a.CreatedBy = rawMessage.CreatedBy
	a.UpdatedAt = time.Unix(int64(rawMessage.UpdatedAt), 0)
	a.UpdatedBy = rawMessage.UpdatedBy
	a.CurrentUserID = rawMessage.CurrentUserID
	a.Country = rawMessage.Country
	a.CustomersMode = rawMessage.CustomersMode
	a.IsUnsorted_on = rawMessage.IsUnsorted_on
	a.IsLossReasonEnabled = rawMessage.IsLossReasonEnabled
	a.IsHelpbotEnabled = rawMessage.IsHelpbotEnabled
	a.IsTechnicalAccount = rawMessage.IsTechnicalAccount
	a.ContactNameDisplayOrder = rawMessage.ContactNameDisplayOrder
	a.AmojoID = rawMessage.AmojoID
	a.Version = rawMessage.Version

	return nil
}



type Pipeline struct {
	ID       int       `json:"id"`       // Уникальный идентификатор воронки
	Name     string    `json:"name"`     // Название воронки
	Sort     int       `json:"sort"`     // Порядковый номер воронки при отображении
	IsMain   bool      `json:"is_main"`  // Является ли воронка “главной”
	Statuses []*Status `json:"statuses"` // Этапы цифровой воронки
}

type Status struct {
	ID       int    `json:"id"`       // Уникальный идентификатор этапа
	Name     string `json:"name"`     // Название этапа
	Sort     int    `json:"sort"`     // Порядковый номер этапа при отображении
	Color    string `json:"color"`    // Цвет этапа (подробнее см. здесь)
	Editable bool   `json:"editable"` // Есть ли возможность изменить или удалить этот этап
}

type UserInfo struct {
	ID          int                       // Уникальный идентификатор пользователя
	Name        string                    // Имя пользователя
	Login       string                    // Логин пользователя
	Language    string                    // Настройки языка пользователя
	PhoneNumber string                    // Номер телефона пользователя
	GroupID     int                       // id группы, в которой состоит пользователь
	IsActive    bool                      // Активна учётная запись пользователя или нет, если нет, то доступ будет закрыт
	IsFree      bool                      // Является ли учётная запись пользователя бесплатной
	IsAdmin     bool                      // Наличие прав администратора
	Rights      map[string]UserPermission // Права пользователя (описание формата см. здесь)
}

type UserPermission uint8

const (
	PermUnknown UserPermission = iota
	PermAllowed
	PermAllowedResponsible
	PermRestricted
	PermAllowedGroup
)

func UserPermissionFromString(in string) UserPermission {
	switch in {
	case "A":
		return PermAllowed
	case "M":
		return PermAllowedResponsible
	case "D":
		return PermRestricted
	case "G":
		return PermAllowedGroup
	default:
		return PermUnknown
	}
}

func AmoDateFormat(in string) string {
	for amoNotation, goNotation := range map[string]string{
		"D": "Mon",
		"d": "02",
		"M": "Jan",
		"m": "01",
		"Y": "2006",
		"H": "15",
		"i": "04",
		"s": "05",
	} {
		in = strings.ReplaceAll(in, amoNotation, goNotation)
	}
	return in
}

/*
func (c *ClientBase) StatusString(pipeline, status int) (string, error) {
	for _, p := range c.accountData.Pipelines {
		if p.ID == pipeline {
			for _, s := range p.Statuses {
				if s.ID == status {
					return s.Name, nil
				}
			}
			return "", errors.New("status " + strconv.Itoa(status) + " in pipeline " + strconv.Itoa(pipeline) + " not found")
		}
	}

	return "", errors.New("pipeline " + strconv.Itoa(pipeline) + " not found")
}

func (c *ClientBase) ParseStauts(pipeline, status string) (int, error) {
	for _, p := range c.accountData.Pipelines {
		if p.Name == pipeline {
			for _, s := range p.Statuses {
				if s.Name == status {
					return s.ID, nil
				}
			}
			return 0, errors.New("status " + status + " in pipeline " + pipeline + " not found")
		}
	}

	return 0, errors.New("pipeline " + pipeline + " not found")
}

func (c *ClientBase) PipelineString(pipeline int) (string, error) {
	for _, p := range c.accountData.Pipelines {
		if p.ID == pipeline {
			return p.Name, nil
		}
	}

	return "", errors.New("pipeline " + strconv.Itoa(pipeline) + " not found")
}

func (c *ClientBase) ParsePipeline(pipeline string) (int, error) {
	for _, p := range c.accountData.Pipelines {
		if p.Name == pipeline {
			return p.ID, nil
		}
	}

	return 0, errors.New("pipeline " + pipeline + " not found")
}
*/
