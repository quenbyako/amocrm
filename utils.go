package amocrm

import (
	"bytes"
	"encoding/csv"
	"fmt"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/fatih/set"

	"github.com/araddon/dateparse"
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/xelaj/go-dry"
)

//! DEPRECATED
func MergeStrings(a, b []string) []string {
	res := make([]string, 0)

	for _, v := range append(a, b...) {
		if !dry.StringInSlice(v, res) {
			res = append(res, v)
		}
	}

	return res
}

//! DEPRECATED
func MergeInts(a, b []int) []int {
	res := make([]int, 0)
	for _, v := range append(a, b...) {
		if !dry.SliceContains(res, v) {
			res = append(res, v)
		}
	}

	return res
}

/*
 * ====== MERGING TOOLS =======
 */

// если clearIfBHasZeroValue == true, то функция НЕ БУДЕТ добавлять в результат значение, если в b оно равно nil или нулевому значению
func Merge(a, b interface{}, clearIfBHasZeroValue bool) (interface{}, error) {
	if reflect.ValueOf(a).IsNil() || a == nil {
		return nil, errors.New("это новый объект, смердживать нет смысла")
	}

	if reflect.TypeOf(a) != reflect.TypeOf(b) {
		return nil, errors.New("типы разные: " + reflect.TypeOf(a).String() + " " + reflect.TypeOf(b).String())
	}

	res := makeZeroObject(reflect.TypeOf(a))

	if reflect.ValueOf(b).IsNil() || b == nil {
		dry.PanicIfErr(copier.Copy(&res, a))
		return res, nil
	}

	aVal := reflect.Indirect(reflect.ValueOf(a))
	bVal := reflect.Indirect(reflect.ValueOf(b))
	rVal := reflect.Indirect(reflect.ValueOf(res))

	for i := 0; i < aVal.NumField(); i++ {
		if !aVal.Field(i).CanSet() { // избавляемся от неэкспортируемых филдов
			continue
		}

		newOne, err := mergeSingleField(aVal.Field(i).Interface(), bVal.Field(i).Interface(), clearIfBHasZeroValue)
		if err != nil {
			return nil, errors.Wrap(err, aVal.Type().Field(i).Name)
		}

		rVal.Field(i).Set(reflect.ValueOf(newOne))
	}

	return res, nil
}

func mergeSingleField(a, b interface{}, clearIfBHasZeroValue bool) (interface{}, error) {
	if b == nil {
		v := makeZeroObject(reflect.TypeOf(a))
		if !clearIfBHasZeroValue {
			dry.PanicIfErr(copier.Copy(&v, a))
		}

		return v, nil
	}

	if reflect.TypeOf(a) != reflect.TypeOf(b) {
		panic("типы разные: " + reflect.TypeOf(a).String() + " " + reflect.TypeOf(b).String())
	}

	if reflect.DeepEqual(a, b) {
		v := makeZeroObject(reflect.TypeOf(a))
		dry.PanicIfErr(copier.Copy(&v, a))
		return v, nil
	}

	if reflect.ValueOf(a).IsZero() {
		v := makeZeroObject(reflect.TypeOf(b))
		dry.PanicIfErr(copier.Copy(&v, b))
		return v, nil
	}

	if reflect.ValueOf(b).IsZero() {
		v := makeZeroObject(reflect.TypeOf(a))
		if !clearIfBHasZeroValue {
			dry.PanicIfErr(copier.Copy(&v, a))
		}

		return v, nil
	}

	aVal := reflect.Indirect(reflect.ValueOf(a))
	bVal := reflect.Indirect(reflect.ValueOf(b))

	switch ar := a.(type) {
	case time.Time:
		br := b.(time.Time)
		if br.IsZero() {
			if clearIfBHasZeroValue {
				return time.Time{}, nil
			}
			return time.Date(
				ar.Year(),
				ar.Month(),
				ar.Day(),
				ar.Hour(),
				ar.Minute(),
				ar.Second(),
				ar.Nanosecond(),
				ar.Location(),
			), nil
		}

		return time.Date(
			br.Year(),
			br.Month(),
			br.Day(),
			br.Hour(),
			br.Minute(),
			br.Second(),
			br.Nanosecond(),
			br.Location(),
		), nil
	}

	switch k := aVal.Type().Kind(); k {
	case reflect.Bool, reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
		reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr,
		reflect.Float32, reflect.Float64, reflect.Complex64, reflect.Complex128, reflect.String:
		// ситуации:
		// (они все исполняются заранее где проверка на ноль, это ж базовые типы)
		// но только в этом кейсе
		//                          false true
		// a: 123, b: 456, false -> 456   456
		// a: 123, b: 0,   false -> 123   0
		// a: 0,   b: 456, false -> 456   456
		// a: 0,   b: 0,   false -> 0     0

		newOne := reflect.New(aVal.Type()).Elem()
		newOne.Set(bVal)
		return newOne.Interface(), nil

	case reflect.Map:
		// ситуации:
		//                            false     true
		// a:{}        b:{a:1}     -> {a:1}     {a:1}
		// a:{b:1}     b:{}        -> {b:1}     {}
		// a:{b:1}     b:{b:2}     -> {b:2}     {b:2}
		// a:{b:1}     b:{a:1}     -> {b:1 a:1} {a:1}
		// a:{b:1}     b:{a:1 b:2} -> {a:1 b:2} {a:1 b:2}
		// a:{a:1}     b:{a:1 b:1} -> {a:1 b:1} {a:1 b:1}
		// a:{a:1}     b:{a:2}     -> {a:2}     {a:2}
		// a:{a:1}     b:{a:2 b:1} -> {a:2 b:1} {a:2 b:1}
		// a:{a:1 b:1} b:{b:1}     -> {a:1 b:1} {b:1}
		// a:{a:1 b:1} b:{b:2}     -> {a:1 b:2} {b:2}
		// a:{a:1 b:1} b:{a:1}     -> {a:1 b:1} {a:1}
		// a:{a:1 b:1} b:{a:1 b:2} -> {a:1 b:2} {a:1 b:2}
		// a:{a:1 b:1} b:{a:2}     -> {a:2 b:1} {a:2}
		// a:{a:1 b:1} b:{a:2 b:1} -> {a:2 b:1} {a:2 b:1}
		// a:{a:1 b:1} b:{a:2 b:2} -> {a:2 b:2} {a:2 b:2}
		aKeys := aVal.MapKeys()
		bKeys := bVal.MapKeys()
		newOne := reflect.MakeMap(aVal.Type())

		for _, key := range bKeys {
			// если ключа нет (_, ok :=... это и есть походу IsValid), то мы 100500% пуляем в новую мапу,
			// потому что че мерджить пустое поле то, правильно?
			if !aVal.MapIndex(key).IsValid() {
				// баг copier: из-за индиректа тупо нарушает целостность указателей, из-за чего интерфейс скопировать нельзя
				//val := reflect.New(bVal.Type().Elem()).Interface()
				//pp.Println(val, reflect.TypeOf(val).String())
				//dry.PanicIfErr(copier.Copy(&val, bVal.MapIndex(key).Interface()))
				//pp.Println(val, reflect.TypeOf(val).String())

				newOne.SetMapIndex(key, bVal.MapIndex(key))
			}
		}

		for _, key := range aKeys {
			//val := makeZeroObject(aVal.Type().Elem())

			// если ключа нет (_, ok :=... это и есть походу IsValid), то мы 100500% пуляем в новую мапу,
			// потому что че мерджить пустое поле то, правильно?
			if !bVal.MapIndex(key).IsValid() {
				if clearIfBHasZeroValue {
					continue
				}

				// баг copier: из-за индиректа тупо нарушает целостность указателей, из-за чего интерфейс скопировать нельзя
				//dry.PanicIfErr(copier.Copy(&val, aVal.MapIndex(key).Interface()))
				newOne.SetMapIndex(key, aVal.MapIndex(key))
				continue
			}
			// баг copier: из-за индиректа тупо нарушает целостность указателей, из-за чего интерфейс скопировать нельзя
			// dry.PanicIfErr(copier.Copy(&val, bVal.MapIndex(key).Interface()))
			newOne.SetMapIndex(key, bVal.MapIndex(key))
		}
		return newOne.Interface(), nil

	case reflect.Slice:
		// поля слайсов мы представляем как набор, поскольку ну камон, так проще
		// ситуации:
		//                    false true
		// a:{}    b:{}    -> {}    {}
		// a:{}    b:{a}   -> {a}   {a}
		// a:{a}   b:{}    -> {a}   {}
		// a:{a}   b:{a}   -> {a}   {a}
		// a:{a}   b:{b}   -> {a b} {b}
		// a:{a}   b:{a b} -> {a b} {a b}
		// a:{b}   b:{a}   -> {b a} {a}
		// a:{a b} b:{a}   -> {a b} {a}
		// a:{a b} b:{a b} -> {a b} {a b}
		//
		// то есть по сути: при false это union, при true всегда берем только b

		if clearIfBHasZeroValue {
			newOne := makeZeroObject(bVal.Type())
			dry.PanicIfErr(copier.Copy(&newOne, bVal.Interface()))
			return newOne, nil
		}

		aSet := set.New(set.NonThreadSafe)
		for i := 0; i < aVal.Len(); i++ {
			aSet.Add(aVal.Index(i).Interface())
		}
		bSet := set.New(set.NonThreadSafe)
		for i := 0; i < bVal.Len(); i++ {
			bSet.Add(bVal.Index(i).Interface())
		}

		list := set.Union(aSet, bSet).List()
		sort.Slice(list, func(i, j int) bool {
			return dry.ReflectLess(list[i], list[j])
		})

		newOne := reflect.MakeSlice(aVal.Type(), len(list), len(list))
		for i, item := range list {
			newOne.Index(i).Set(reflect.ValueOf(item))
		}

		return newOne.Interface(), nil

	default:
		return nil, errors.New("unknown type: " + aVal.Type().String())
	}
}

func ListToString(in []string) string {
	buf := bytes.NewBuffer([]byte{})
	csvwriter := csv.NewWriter(buf)
	dry.PanicIfErr(csvwriter.Write(in))
	csvwriter.Flush()
	str := buf.String()
	return str[:len(str)-1] // убираем бесполезный перевод строки, потому что csv не отключается эта штука
}

func StringToList(in string) []string {
	if in == "" {
		return []string{}
	}
	buf := bytes.NewBufferString(in)
	csvreader := csv.NewReader(buf)
	res, err := csvreader.Read()
	if err != nil {
		return []string{in}
	}
	if res == nil {
		res = []string{}
	}

	return res
}

func stringifyWithParams(items ...fmt.Stringer) string {
	stringedWith := make([]string, len(items))
	for i, item := range items {
		stringedWith[i] = item.String()
	}
	return ListToString(stringedWith)
}

// пытается разделить строчку на набор чисел. может выдать либо слайс из строк либо nil
func SplitStringInts(in string) ([]int, error) {
	pre := StringToList(in)
	if len(pre) == 0 {
		return []int{}, nil
	}
	res := make([]int, len(pre))
	var err error
	for i, item := range pre {
		res[i], err = strconv.Atoi(item)
		if err != nil {
			return nil, errors.Wrapf(err, "[%v]", i)
		}
	}
	return res, nil
}

func utcParseDate(t string) (time.Time, error) {
	res, err := dateparse.ParseIn(t, time.UTC)
	if err != nil {
		return time.Time{}, err
	}
	return res, nil
}

func parseInt(t string) (int, error) {
	t = strings.Trim(t, " \n\t")
	if t == "" {
		return 0, nil
	}
	return strconv.Atoi(t)
}

func makeZeroObject(t reflect.Type) interface{} {
	switch k := t.Kind(); k {
	case reflect.Bool, reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
		reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr,
		reflect.Float32, reflect.Float64, reflect.Complex64, reflect.Complex128, reflect.String,
		reflect.Struct:
		return reflect.New(t).Elem().Interface()

	case reflect.Map:
		return reflect.MakeMap(t).Interface()

	case reflect.Slice:
		return reflect.MakeSlice(t, 0, 0).Interface()

	case reflect.Interface:
		return reflect.New(t).Interface()

	case reflect.Ptr:
		return reflect.New(t.Elem()).Interface()

	default:
		panic("unknown kind: " + k.String())
	}
}

func findDescriptionByName(items []*CustomFieldDescription, name string) (*CustomFieldDescription, bool) {
	for _, item := range items {
		if strings.EqualFold(item.Name, name) {
			return item, true
		}
	}

	return nil, false
}
