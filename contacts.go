package amocrm

import (
	"fmt"
	"reflect"
	"sort"
	"strings"
	"time"

	"github.com/pkg/errors"
)

// Contact описывает тип контакта из AmoCRM
//
// Поскольку в выводе контакта отдается бесполезная (для обычного использования) информация о custom_fields
// которая может пригождаться для обработки эвентов (в эвентах названия полей не выдаются), то приходится при декодировании
// сохранять связку fueld_id->field_name во время парсинга. после парсинга Client обязан вызывать метод getFieldIDs, чтобы из контакта они пропали
// костыль жуткий, но он поможет писать поменьше кода, чем если бы мы сохраняли полную структуру из ответа амы
type Contact struct {
	ID              int              `csv:"id"`
	Name            string           `csv:"name"`
	FirstName       string           `csv:"first_name"`
	LastName        string           `csv:"last_name"`
	ResponsibleUser int              `csv:"responsible_user_id"`
	GroupID         int              `csv:"group_id"`
	CreatedBy       int              `csv:"created_by"`
	UpdatedBy       int              `csv:"updated_by"`
	CreatedAt       time.Time        `csv:"created_at"`
	UpdatedAt       time.Time        `csv:"updated_at"`
	ClosestTaskAt   time.Time        `csv:"closest_task_at"`
	Fields          map[string]Field `csv:"custom_field"`
	AccountID       int              `csv:"account_id"`
	Companies       []int            `csv:"company"`
	Leads           []int            `csv:"leads"`
	Tags            []string         `csv:"tags"`
	Customers       []int            `csv:"customers"`
	client          *ClientSpecific  `csv:"-" pp:"-"`
}

func (c *Contact) AmoID() int {
	return c.ID
}

func (c *Contact) GetName() string {
	return c.Name
}

func (c *Contact) Type() string {
	return "contact"
}

/*
// Merge пытается замерджить один контакт с другим, ставя в приоритет данные из контакта, у которого был вызван метод
// например, contact1.Merge(contact2) даст приоритет в данных contact1
// 1: amo 2: table 3: amo->table
func (c1 *Contact) Merge(c2 *Contact) (*Contact, error) {
	if c2 == nil {
		return nil, errors.New("can't merge nil contact")
	}

	var res *Contact

	tmp := *c2
	res = &tmp

	if c1 == nil {
		return res, nil
	}

	if c1.ID != c2.ID {
		return nil, errors.New("аккаунты имеют разные amoID, и не могут быть смерджены")
	}

	isEqual, err := c1.isEqualSheetsID(c2)
	switch {
	case err != nil:
		return nil, err
	case !isEqual:
		return nil, errors.New("аккаунты имеют разные GoogleSheetID, и не могут быть смерджены")
	}

	// TODO ЭТО ГОВНОКОД! переделать если будет время. по факту это хуйня полная а не код
	if c1.Name != "" {
		if c1.Name != c2.Name {
			res.Name = c1.Name
		}
	}

	if c1.FirstName != "" && c1.FirstName != c2.FirstName {
		res.FirstName = c1.FirstName
	}

	if c1.LastName != "" && c1.LastName != c2.LastName {
		res.LastName = c1.LastName
	}

	if c1.ResponsibleUser != 0 && c1.ResponsibleUser != c2.ResponsibleUser {
		res.ResponsibleUser = c1.ResponsibleUser
	}

	if c1.CreatedBy != 0 && c1.CreatedBy != c2.CreatedBy {
		res.CreatedBy = c1.CreatedBy
	}

	if c1.CreatedAt.Unix() != 0 && c1.CreatedAt.Unix() != c2.CreatedAt.Unix() {
		res.CreatedAt = c1.CreatedAt
	}

	if c1.UpdatedAt.Unix() != 0 && c1.UpdatedAt.Unix() != c2.UpdatedAt.Unix() {
		res.UpdatedAt = c1.UpdatedAt
	}

	if c1.AccountID != 0 && c1.AccountID != c2.AccountID {
		res.AccountID = c1.AccountID
	}

	if c1.UpdatedBy != 0 && c1.UpdatedBy != c2.UpdatedBy {
		res.UpdatedBy = c1.UpdatedBy
	}

	if c1.GroupID != 0 && c1.GroupID != c2.GroupID {
		res.GroupID = c1.GroupID
	}

	//if c1.Company != nil && !reflect.DeepEqual(c1.Company, c2.Company) {
	//	res.Company = c1.Company
	//}

	if c1.Leads != nil && len(c1.Leads) != 0 {
		res.Leads = MergeInts(c1.Leads, c2.Leads)
	}

	if c1.ClosestTaskAt.Unix() != 0 && c1.ClosestTaskAt.Unix() != c2.ClosestTaskAt.Unix() {
		res.ClosestTaskAt = c1.ClosestTaskAt
	}

	if c1.Tags != nil && len(c1.Tags) != 0 {
		res.Tags = MergeStrings(c1.Tags, c2.Tags)
	}

	// TODO написать тесты
	if c1.Fields != nil && len(c1.Fields) != 0 {
		if res.Fields == nil {
			res.Fields = make(map[string]Field)
		}

		for k, v1 := range c1.Fields {
			if v1 == nil {
				continue
			}

			//v2, ok := c2.Fields[k]
			//if !ok {
			//	res.Fields[k] = v1
			//	continue
			//}
			//
			//res.Fields[k] = MergeStrings(v1, v2)
			res.Fields[k] = v1 // TODO документация амы не описывает конкретно, как именно получать типы филдов, будем считать что он если меняется то целиком
			//                    но надо понять, как правильно это можно обработать
		}
	}

	if c1.Customers != nil && len(c1.Customers) != 0 {
		res.Customers = MergeInts(c1.Customers, c2.Customers)
	}

	return res, nil
}

// 1: amo 2: table 3: table->amo
func (c1 *Contact) MergeTable(c2 *Contact) (*Contact, error) {
	if c2 == nil {
		return nil, errors.New("can't merge nil contact")
	}

	var res *Contact

	tmp := *c2
	res = &tmp

	if c1 == nil {
		return res, nil
	}

	if c1.ID != c2.ID {
		return nil, errors.New("аккаунты имеют разные amoID, и не могут быть смерджены")
	}

	isEqual, err := c1.isEqualSheetsID(c2)
	switch {
	case err != nil:
		return nil, err
	case !isEqual:
		return nil, errors.New("аккаунты имеют разные GoogleSheetID, и не могут быть смерджены")
	}

	if c1.Name != "" && c1.Name != c2.Name {
		res.Name = c2.Name
	} else {
		res.Name = c1.Name
	}

	if c1.FirstName != c2.FirstName {
		res.FirstName = c2.FirstName
	} else {
		res.FirstName = c1.FirstName
	}

	if c1.LastName != "" && c1.LastName != c2.LastName {
		res.LastName = c2.LastName
	} else {
		res.LastName = c1.LastName
	}

	if c1.ResponsibleUser != 0 && c1.ResponsibleUser != c2.ResponsibleUser {
		res.ResponsibleUser = c2.ResponsibleUser
	} else {
		res.ResponsibleUser = c1.ResponsibleUser
	}

	if c1.CreatedBy != 0 && c1.CreatedBy != c2.CreatedBy {
		res.CreatedBy = c2.CreatedBy
	} else {
		res.CreatedBy = c1.CreatedBy
	}

	if c1.CreatedAt.Unix() != 0 && c1.CreatedAt.Unix() != c2.CreatedAt.Unix() {
		res.CreatedAt = c2.CreatedAt
	} else {
		res.CreatedAt = c1.CreatedAt
	}

	if c1.UpdatedAt.Unix() != 0 && c1.UpdatedAt.Unix() != c2.UpdatedAt.Unix() {
		res.UpdatedAt = c2.UpdatedAt
	} else {
		res.UpdatedAt = c1.UpdatedAt
	}

	if c1.AccountID != 0 && c1.AccountID != c2.AccountID {
		res.AccountID = c2.AccountID
	} else {
		res.AccountID = c1.AccountID
	}

	if c1.UpdatedBy != 0 && c1.UpdatedBy != c2.UpdatedBy {
		res.UpdatedBy = c2.UpdatedBy
	} else {
		res.UpdatedBy = c1.UpdatedBy
	}

	if c1.GroupID != 0 && c1.GroupID != c2.GroupID {
		res.GroupID = c2.GroupID
	} else {
		res.GroupID = c1.GroupID
	}

	if c1.Company != nil && !reflect.DeepEqual(c1.Company, c2.Company) {
		res.Company = c2.Company
	} else {
		res.Company = c1.Company
	}

	if c1.Leads != nil {
		res.Leads = c1.Leads
	}

	if c1.ClosestTaskAt.Unix() != 0 && c1.ClosestTaskAt.Unix() != c2.ClosestTaskAt.Unix() {
		res.ClosestTaskAt = c2.ClosestTaskAt
	} else {
		res.ClosestTaskAt = c1.ClosestTaskAt
	}

	if c1.Tags != nil {
		res.Tags = c1.Tags
	}

	// TODO написать тесты
	res.Fields = make(map[string][]string)
	if c1.Fields != nil && len(c1.Fields) != 0 {
		for k, v1 := range c1.Fields {
			if v1 == nil {
				continue
			}

			v2, ok := c2.Fields[k]
			if ok {
				res.Fields[k] = v2
				continue
			}

			res.Fields[k] = v1 // TODO документация амы не описывает конкретно, как именно получать типы филдов, будем считать что он если меняется то целиком
			//                но надо понять, как правильно это можно обработать
		}
	}

	if c1.Customers != nil {
		res.Customers = c1.Customers
	}

	return res, nil
}
*/

// amo, table, amo -> table
func (c1 *Contact) MergeToTable(c2 *Contact) (*Contact, error) {
	return c1.merge(c2, false)
}

// table, amo, table -> amo
func (c1 *Contact) MergeToAmo(c2 *Contact) (*Contact, error) {
	return c1.merge(c2, true)
}

func (c1 *Contact) merge(c2 *Contact, clearIfBHasZeroValue bool) (*Contact, error) {
	if c1.client != c2.client {
		return nil, errors.New("contacts has different clients")
	}
	if c1.ID != c2.ID {
		return nil, errors.New("contacts has different amo ids")
	}

	merged, err := Merge(c1, c2, clearIfBHasZeroValue)
	if err != nil {
		return nil, errors.Wrapf(err, "merging contact %v", c1.ID)
	}
	res := merged.(*Contact)
	res.client = c1.client
	return res, nil
}

/*

func (c *Contact) GoogleSheetID() (int, error) {
	if v, ok := c.Fields[GoogleSheetIDFieldName]; ok {
		res, err := strconv.Atoi(v[0])
		if err != nil {
			return 0, &ErrGoogleSheetIDInvalidData{
				FieldName:  GoogleSheetIDFieldName,
				AmoID:      c.AccountID,
				FieldValue: v[0],
			}
		}
		return res, nil
	}

	return 0, &ErrGoogleSheetIDNotFound{}
}

type Company struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

func (c *Company) String() string {
	return c.Name
}

func (c *Company) GetID() int {
	return c.ID
}
*/

func (c *Contact) ResetClient(client *ClientSpecific) {
	c.client = client
}

func (c *Contact) Hash() string {
	prev := strings.Join(SprintfAllItems(
		c.ID,
		c.Name,
		c.FirstName,
		c.LastName,
		c.ResponsibleUser,
		c.CreatedBy,
		c.CreatedAt.Unix(),
		c.UpdatedAt.Unix(),
		c.AccountID,
		c.UpdatedBy,
		c.GroupID,
		sortInts(c.Leads),
		c.ClosestTaskAt.Unix(),
		sortStrings(c.Tags),
		kvFromMap(c.Fields).String(),
		c.Customers,
	), ";")

	return prev
	//hash := md5.Sum([]byte(prev))
	//return hex.EncodeToString(hash[:])
}

func sortInts(in []int) []int {
	l := make([]int, len(in))
	copy(l, in)
	sort.Ints(l)
	return l
}

func sortStrings(in []string) []string {
	l := make([]string, len(in))
	copy(l, in)
	sort.Strings(l)
	return l
}

type kv struct {
	k interface{}
	v interface{}
}

func (k *kv) String() string {
	return fmt.Sprintf("%v: %v", k.k, k.v)
}

type kvs []kv

func kvFromMap(in interface{}) *kvs {
	rval := reflect.ValueOf(in)
	if rval.Type().Kind() != reflect.Map {
		panic("not a map")
	}

	res := make(kvs, 0, rval.Len())
	for _, key := range rval.MapKeys() {
		res = append(res, kv{key.Interface(), rval.MapIndex(key).Interface()})
	}

	return &res
}

func (k kvs) String() string {
	sort.Slice(k, func(i, j int) bool {
		return fmt.Sprint(k[i].k) < fmt.Sprint(k[j].k)
	})

	strs := make([]string, 0, len(k))
	for _, item := range k {
		strs = append(strs, item.String())
	}
	return strings.Join(strs, "; ")
}

func SprintfAllItems(in ...interface{}) []string {
	res := make([]string, len(in))
	for i, item := range in {
		res[i] = fmt.Sprint(item)
	}
	return res
}

/*
func sortStringsAndConcatenate(in []string) string {
	l := make([]string, len(in))
	copy(l, in)
	sort.Strings(l)
	return strings.Join(l, ",")
}
*/

func (c *Contact) FilterCustomFields(f func(string) bool) {
	for k := range c.Fields {
		if !f(k) {
			delete(c.Fields, k)
		}
	}
}

func (c *Contact) GetCustomFields() map[string]Field {
	return c.Fields
}
