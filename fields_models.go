package amocrm

import (
	"encoding/json"
	"fmt"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/k0kubun/pp"

	"github.com/araddon/dateparse"

	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"github.com/xelaj/go-dry"
)

type CustomFieldType uint8

const (
	FieldTypeUnknown       CustomFieldType = iota
	FieldTypeText                          // Обычное текстовое поле
	FieldTypeNumeric                       // Текстовое поле с возможностью передавать только цифры
	FieldTypeCheckbox                      // Поле обозначающее только наличие или отсутствие свойства (например: “да”/”нет”)
	FieldTypeSelect                        // Поле типа список с возможностью выбора одного элемента
	FieldTypeMultiselect                   // Поле типа список c возможностью выбора нескольких элементов списка
	FieldTypeDate                          // Поле типа дата возвращает и принимает значения в формате (Y-m-d H:i:s)
	FieldTypeUrl                           // Обычное текстовое поле предназначенное для ввода URL адресов
	FieldTypeTextArea                      // Поле textarea содержащее большое количество текста
	FieldTypeRadioButton                   // Поле типа переключатель
	FieldTypeStreetaddress                 // Короткое поле адрес
	FieldTypeSmartAddress                  // Поле адрес (в интерфейсе является набором из нескольких полей)
	FieldTypeMultitext                     // мультитекст, очень странное поле хочу вам сказать
	FieldTypeBirthday                      // Поле типа дата поиск по которому осуществляется без учета года, значения в формате (Y-m-d H:i:s)
	FieldTypeLegalEntity                   // Поле юридическое лицо (в интерфейсе является набором из нескольких полей)
	FieldTypeDateTime                      // поле дата + время
)

func GetCustomFieldType(from string) CustomFieldType {
	switch from {
	case "text":
		return FieldTypeText
	case "numeric":
		return FieldTypeNumeric
	case "checkbox":
		return FieldTypeCheckbox
	case "select":
		return FieldTypeSelect
	case "multiselect":
		return FieldTypeMultiselect
	case "date":
		return FieldTypeDate
	case "url":
		return FieldTypeUrl
	case "textarea":
		return FieldTypeTextArea
	case "radiobutton":
		return FieldTypeRadioButton
	case "streetaddress":
		return FieldTypeStreetaddress
	case "smart_address":
		return FieldTypeSmartAddress
	case "multitext":
		return FieldTypeMultitext
	case "birthday":
		return FieldTypeBirthday
	case "legal_entity":
		return FieldTypeLegalEntity
	case "date_time":
		return FieldTypeDateTime
	default:
		return FieldTypeUnknown
	}
}

func (c CustomFieldType) String() string {
	return []string{
		"<UNKNOWN>",
		"text",
		"numeric",
		"checkbox",
		"select",
		"multiselect",
		"date",
		"url",
		"textarea",
		"radiobutton",
		"streetaddress",
		"smart_address",
		"multitext",
		"birthday",
		"legal_entity",
		"date_time",
	}[c]
}

func GenerateFieldObjectByType(as CustomFieldType) (Field, error) {
	switch as {
	case FieldTypeText:
		f := TextField("")
		return &f, nil
	case FieldTypeNumeric:
		f := NumericField(0)
		return &f, nil
	case FieldTypeCheckbox:
		f := CheckboxField(false)
		return &f, nil
	case FieldTypeSelect:
		return &SelectField{}, nil
	case FieldTypeMultiselect:
		return &MultiselectField{}, nil
	case FieldTypeDate:
		return &DateField{}, nil
	case FieldTypeUrl:
		f := UrlField("")
		return &f, nil
	case FieldTypeTextArea:
		f := TextareaField("")
		return &f, nil
	case FieldTypeRadioButton:
		return &RadiobuttonField{}, nil
	case FieldTypeStreetaddress:
		f := StreetAddressField("")
		return &f, nil
	case FieldTypeSmartAddress:
		return &SmartAddressField{}, nil
	case FieldTypeMultitext:
		return &MultitextField{}, nil
	case FieldTypeBirthday:
		return &BirthdayField{}, nil
	case FieldTypeLegalEntity:
		return &LegalEntityField{}, nil
	case FieldTypeDateTime:
		return &DateTimeField{}, nil
	default:
		return nil, fmt.Errorf("unknown type %d (check out enu, id, can't get it's name)", as)
	}
}

type Field interface {
	UnmarshalJSON(b []byte) error
	MarshalJSON() ([]byte, error)
	UnmarshalText(b []byte) error
	String() string
}

type TextField string

func (f *TextField) UnmarshalJSON(b []byte) error {
	var res []fieldItem

	err := json.Unmarshal(b, &res)
	if err != nil {
		return err
	}

	if len(res) != 1 {
		return errors.New("field count is not 1")
	}

	*f = TextField(res[0].Value.(string))

	return nil
}

func (f *TextField) MarshalJSON() ([]byte, error) {
	res := []fieldItem{
		{
			Value: *f,
		},
	}

	return json.Marshal(res)
}

func (f *TextField) UnmarshalText(b []byte) error {
	*f = TextField(b)
	return nil
}

func (f *TextField) String() string {
	return string(*f)
}

type NumericField float64

func (f *NumericField) UnmarshalJSON(b []byte) error {
	var res []fieldItem

	err := json.Unmarshal(b, &res)
	if err != nil {
		return err
	}

	if len(res) != 1 {
		return errors.New("field count is not 1")
	}

	value, err := strconv.ParseFloat(res[0].Value.(string), 64)
	if err != nil {
		return errors.Wrap(err, "converting string to number")
	}

	*f = NumericField(value)

	return nil
}

func (f *NumericField) MarshalJSON() ([]byte, error) {
	res := []fieldItem{
		{
			Value: *f,
		},
	}

	return json.Marshal(res)
}

func (f *NumericField) UnmarshalText(b []byte) error {
	i, err := strconv.ParseFloat(string(b), 64)
	if err != nil {
		return err
	}

	*f = NumericField(i)
	return nil
}

func (f *NumericField) String() string {
	return strconv.FormatFloat(float64(*f), 'f', -1, 64)
}

type CheckboxField bool

func (f *CheckboxField) UnmarshalJSON(b []byte) error {
	var res []fieldItem

	err := json.Unmarshal(b, &res)
	if err != nil {
		return err
	}

	if len(res) != 1 {
		return errors.New("field count is not 1")
	}

	*f = CheckboxField(res[0].Value.(bool))

	return nil
}

func (f *CheckboxField) MarshalJSON() ([]byte, error) {
	res := []fieldItem{
		{
			Value: *f,
		},
	}

	return json.Marshal(res)
}

func (f *CheckboxField) UnmarshalText(b []byte) error {
	raw := strings.ToLower(string(b))
	switch raw {
	case "1", "t", "y", "yes", "true", "on", "истина", "правда", "✅", "✓", "🗸":
		*f = true
	case "0", "f", "n", "no", "false", "off", "ложь", "❌", "❎":
		*f = false
	default:
		return errors.New("non explicit boolean value: '" + string(b) + "'")
	}

	return nil
}

func (f *CheckboxField) String() string {
	return strconv.FormatBool(bool(*f))
}

type SelectField struct {
	Value  string
	EnumID int
}

func (f *SelectField) UnmarshalJSON(b []byte) error {
	var res []fieldItem

	err := json.Unmarshal(b, &res)
	if err != nil {
		return err
	}

	if len(res) != 1 {
		return errors.New("field count is not 1")
	}

	var ok bool
	f.Value, ok = res[0].Value.(string)
	if !ok {
		return errors.New("'value' is not string")
	}
	f.EnumID = res[0].EnumID

	return nil
}

// NOTE: перед маршалингом для обновлений нужно ОБЯЗАТЕЛЬНО указать enumID перед тем как пушить
func (f *SelectField) MarshalJSON() ([]byte, error) {
	res := []fieldItem{
		{
			Value:  f.Value,
			EnumID: f.EnumID,
		},
	}

	return json.Marshal(res)
}

func (f *SelectField) UnmarshalText(b []byte) error {
	f.Value = string(b)
	return nil
}

func (f *SelectField) String() string {
	return f.Value
}

type MultiselectField []*SelectField

func (f *MultiselectField) UnmarshalJSON(b []byte) error {
	var res []fieldItem

	err := json.Unmarshal(b, &res)
	if err != nil {
		return err
	}

	*f = make([]*SelectField, len(res))

	for i, item := range res {
		val, ok := item.Value.(string)
		if !ok {
			return fmt.Errorf("[%v]'value' is not string", i)
		}

		(*f)[i] = &SelectField{
			Value:  val,
			EnumID: item.EnumID,
		}
	}

	return nil
}

// NOTE: перед маршалингом для обновлений нужно ОБЯЗАТЕЛЬНО указать enumID перед тем как пушить
func (f *MultiselectField) MarshalJSON() ([]byte, error) {
	res := make([]fieldItem, len(*f))

	for i, item := range *f {
		res[i] = fieldItem{
			EnumID: item.EnumID,
			Value:  item.Value,
		}
	}

	return json.Marshal(res)
}

func (f *MultiselectField) UnmarshalText(b []byte) error {
	items := StringToList(string(b))

	fields := make([]*SelectField, len(items))
	for i, item := range items {
		fields[i] = &SelectField{
			Value:  item,
			EnumID: 0, // TODO: придумать как вытащить отсюдова enumID
		}
	}

	return nil
}

func (f *MultiselectField) String() string {
	items := make([]string, len(*f))
	for i, elem := range *f {
		items[i] = elem.String()
	}

	return strings.Join(items, ",")
}

type DateField struct {
	DateTimeField
}

type UrlField string

func (f *UrlField) UnmarshalJSON(b []byte) error {
	var res []fieldItem

	err := json.Unmarshal(b, &res)
	if err != nil {
		return err
	}

	if len(res) != 1 {
		return errors.New("field count is not 1")
	}

	*f = UrlField(res[0].Value.(string))

	return nil
}

func (f *UrlField) MarshalJSON() ([]byte, error) {
	res := []fieldItem{
		{
			Value: *f,
		},
	}

	return json.Marshal(res)
}

func (f *UrlField) UnmarshalText(b []byte) error {
	*f = UrlField(b)
	return nil
}

func (f *UrlField) String() string {
	return string(*f)
}

type TextareaField string

func (f *TextareaField) UnmarshalJSON(b []byte) error {
	var res []fieldItem

	err := json.Unmarshal(b, &res)
	if err != nil {
		return err
	}

	if len(res) != 1 {
		return errors.New("field count is not 1")
	}

	*f = TextareaField(res[0].Value.(string))

	return nil
}

func (f *TextareaField) MarshalJSON() ([]byte, error) {
	res := []fieldItem{
		{
			Value: *f,
		},
	}

	return json.Marshal(res)
}

func (f *TextareaField) UnmarshalText(b []byte) error {
	*f = TextareaField(b)
	return nil
}

func (f *TextareaField) String() string {
	return string(*f)
}

// actually it works like select field, so we don't need anything more
type RadiobuttonField struct {
	SelectField
}

type StreetAddressField string

func (f *StreetAddressField) UnmarshalJSON(b []byte) error {
	var res []fieldItem

	err := json.Unmarshal(b, &res)
	if err != nil {
		return err
	}

	if len(res) != 1 {
		return errors.New("field count is not 1")
	}

	*f = StreetAddressField(res[0].Value.(string))

	return nil
}

func (f *StreetAddressField) MarshalJSON() ([]byte, error) {
	res := []fieldItem{
		{
			Value: *f,
		},
	}

	return json.Marshal(res)
}

func (f *StreetAddressField) UnmarshalText(b []byte) error {
	*f = StreetAddressField(b)
	return nil
}

func (f *StreetAddressField) String() string {
	return string(*f)
}

type SmartAddressField struct {
	AddressLine1 string `enum_code:"address_line_1"`
	AddressLine2 string `enum_code:"address_line_2"`
	City         string `enum_code:"city"`
	State        string `enum_code:"state"`
	ZipCode      string `enum_code:"zip"`
	CountryCode  string `enum_code:"country"`
}

func (f *SmartAddressField) UnmarshalJSON(b []byte) error {
	var res []fieldItem

	err := json.Unmarshal(b, &res)
	if err != nil {
		return err
	}

	items := map[string]string{}
	for _, item := range res {
		items[item.EnumCode] = fmt.Sprint(item.Value)
	}

	decoder, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		Result:      f,
		ErrorUnused: true,
		TagName:     "enum_code",
	})
	dry.PanicIfErr(err)

	return decoder.Decode(items)
}

func (f *SmartAddressField) MarshalJSON() ([]byte, error) {
	res := []fieldItem{
		{
			EnumCode: "address_line_1",
			Value:    f.AddressLine1,
		},
		{
			EnumCode: "address_line_2",
			Value:    f.AddressLine2,
		},
		{
			EnumCode: "city",
			Value:    f.City,
		},
		{
			EnumCode: "state",
			Value:    f.State,
		},
		{
			EnumCode: "zip",
			Value:    f.ZipCode,
		},
		{
			EnumCode: "country",
			Value:    f.CountryCode,
		},
	}

	return json.Marshal(res)
}

func (f *SmartAddressField) UnmarshalText(b []byte) error {
	items := StringToList(string(b))

	newOne := make([]string, 6)
	copy(newOne, items)

	f.AddressLine1 = newOne[0]
	f.AddressLine2 = newOne[1]
	f.City = newOne[2]
	f.State = newOne[3]
	f.ZipCode = newOne[4]
	f.CountryCode = newOne[5]

	return nil
}

func (f *SmartAddressField) String() string {
	return ListToString([]string{
		f.AddressLine1, f.AddressLine2, f.City, f.State, f.ZipCode, f.CountryCode,
	})
}

type MultitextField map[string]string

func (f *MultitextField) UnmarshalJSON(b []byte) error {
	var res []fieldItem

	err := json.Unmarshal(b, &res)
	if err != nil {
		return err
	}

	for _, item := range res {
		(*f)[item.EnumCode] = fmt.Sprint(item.Value)
	}

	return nil
}

func (f *MultitextField) MarshalJSON() ([]byte, error) {
	items := make([]fieldItem, len(*f))
	i := 0
	for k, v := range *f {
		items[i] = fieldItem{
			EnumCode: k,
			Value:    v,
		}
		i++
	}

	data, err := json.Marshal(items)
	dry.PanicIfErr(err)
	return data, nil
}

func (f *MultitextField) UnmarshalText(b []byte) error {
	items := StringToList(string(b))
	for _, item := range items {
		splitted := strings.SplitN(item, ":", 2)
		k := splitted[0]
		v := ""
		if len(splitted) == 2 {
			v = splitted[1]
		}
		(*f)[k] = v
	}

	return nil
}

func (f *MultitextField) String() string {
	sortedKeys := dry.MapKeys(*f).([]string)
	sort.Strings(sortedKeys)

	for i, key := range sortedKeys {
		sortedKeys[i] = key + ":" + (*f)[key]
	}

	return ListToString(sortedKeys)
}

type BirthdayField struct {
	DateTimeField
}

type LegalEntityField struct {
	Name                      string      `json:"name"`
	EntityType                interface{} `json:"entity_type"`
	VatID                     string      `json:"vat_id"`
	TaxRegistrationReasonCode interface{} `json:"tax_registration_reason_code"`
	Address                   interface{} `json:"address"`
	KPP                       string      `json:"kpp"`
	ExternalUID               interface{} `json:"external_uid"`
}

func (f *LegalEntityField) UnmarshalJSON(b []byte) error {
	var res []fieldItem

	err := json.Unmarshal(b, &res)
	if err != nil {
		return err
	}

	if len(res) != 1 {
		return errors.New("field count is not 1")
	}

	decoder, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		Result:      f,
		ErrorUnused: true,
		TagName:     "json",
	})
	dry.PanicIfErr(err)

	return decoder.Decode(res[0].Value)
}

func (f *LegalEntityField) MarshalJSON() ([]byte, error) {
	panic("not implemented")
}

func (f *LegalEntityField) UnmarshalText(b []byte) error {
	panic("not implemented")
}

func (f *LegalEntityField) String() string {
	panic("not implemented")
}

type DateTimeField struct {
	time.Time
}

func (f *DateTimeField) UnmarshalJSON(b []byte) error {
	var res []fieldItem

	err := json.Unmarshal(b, &res)
	if err != nil {
		return err
	}

	if len(res) != 1 {
		return errors.New("field count is not 1")
	}

	floaty, ok := res[0].Value.(float64)
	if !ok {
		return errors.New("value is not numeric")
	}

	f.Time = time.Unix(int64(floaty), 0)

	return nil
}

func (f *DateTimeField) MarshalJSON() ([]byte, error) {
	res := []fieldItem{
		{
			Value: f.Unix(),
		},
	}

	data, err := json.Marshal(res)
	dry.PanicIfErr(err)
	return data, nil
}

func (f *DateTimeField) UnmarshalText(b []byte) error {
	i, err := dateparse.ParseAny(string(b))
	if err != nil {
		return err
	}
	f.Time = i
	return nil
}

func (f *DateTimeField) String() string {
	return f.Format(TimeFormatInSheets)
}

//==========================================================================================================//

type fieldItem struct {
	Value    interface{} `json:"value,omitempty"` // потому что legal_entity внезапно не строка
	EnumID   int         `json:"enum_id,omitempty"`
	EnumCode string      `json:"enum_code,omitempty"`
}

type rawField struct {
	ID     int             `json:"field_id,omitempty"`
	Name   string          `json:"field_name,omitempty"`
	Code   interface{}     `json:"field_code,omitempty"`
	Type   string          `json:"field_type,omitempty"`
	Values json.RawMessage `json:"values,omitempty"`
}

// Поскольку ответ сервера чрезмерно замудренен (а нам только название и сам филд нужен), эта функция сделана
// сделать жизнь слегка проще. Такие дела
func unmarshalCustomFields(b []byte) (map[string]Field, error) {
	data := make([]*rawField, 0)
	err := json.Unmarshal(b, &data)
	if err != nil {
		pp.Println(string(b))
		return nil, errors.Wrap(err, "unmarshalling")
	}

	result := make(map[string]Field)

	for _, item := range data {
		typ := GetCustomFieldType(item.Type)
		if typ == FieldTypeUnknown {
			return nil, errors.New("'" + item.Type + "' is invalid type")
		}

		name := item.Name
		var realField Field
		realField, err = GenerateFieldObjectByType(typ)
		if err != nil {
			return nil, errors.Wrap(err, name)
		}

		// при анмаршале возможно нужно сетапить филды для корректного парсинга
		// switch f := realField.(type) {
		// }

		err = realField.UnmarshalJSON(item.Values)
		if err != nil {
			return nil, errors.Wrap(err, name)
		}

		result[name] = realField
	}

	return result, nil
}

func (c *ClientSpecific) marshalFieldsForUpdates(entityType string, fields map[string]Field) (json.RawMessage, error) {
	var fieldsDescription []*CustomFieldDescription
	var err error
	switch entityType {
	case "contact":
		fieldsDescription, err = c.GetContactFields()
	case "lead":
		fieldsDescription, err = c.GetContactFields()
	default:
		panic("unsupported entity type: " + entityType)
	}
	if err != nil {
		return nil, errors.Wrap(err, "getting custom fields description")
	}

	res := make([]rawField, len(fields))
	i := 0
	for fieldname, field := range fields {
		choosedDescription, found := findDescriptionByName(fieldsDescription, fieldname)
		if !found {
			return nil, errors.New("description for '" + fieldname + "' not found")
		}

		switch f := field.(type) {
		case *SelectField:
			if f.EnumID == 0 {
				var got bool
				for _, enum := range choosedDescription.Enums {
					if enum.Value == f.Value {
						f.EnumID = enum.ID
						got = true
					}
				}
				if !got {
					return nil, errors.New("enum value not found")
				}
			}
		case *MultiselectField:
			for _, selection := range *f {
				if selection.EnumID == 0 {
					var got bool
					for _, enum := range choosedDescription.Enums {
						if enum.Value == selection.Value {
							selection.EnumID = enum.ID
							got = true
						}
					}
					if !got {
						return nil, errors.New("enum value not found")
					}
				}
			}
		}

		values, err := field.MarshalJSON()
		if err != nil {
			panic(errors.Wrap(err, "field marshalers can't return error! "+reflect.TypeOf(field).String()))
		}

		res[i] = rawField{
			ID:     choosedDescription.ID,
			Name:   choosedDescription.Name,
			Values: json.RawMessage(values),
		}

		i++
	}

	data, err := json.Marshal(res)
	dry.PanicIfErr(err)

	return json.RawMessage(data), nil
}

