package amocrm

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/k0kubun/pp"
	"github.com/pkg/errors"
	"golang.org/x/oauth2"
)

var (
	OauthEndpoint = oauth2.Endpoint{
		AuthURL:  "https://www.amocrm.ru/oauth",
		TokenURL: "https://amocrm.ru/oauth2/access_token", //! you MUST update subdomain for each user. Amo-specific thing
	}
)

func (c *ClientBase) MakeAuthLink(userState string) string {
	link, _ := url.Parse(OauthEndpoint.AuthURL)
	query := url.Values{
		"client_id": []string{c.ClientID},
		"state":     []string{userState},
		"mode":      []string{"post_message"},
	}
	link.RawQuery = query.Encode()

	return link.String()
}

type accessTokenPost struct {
	ClientID     string `json:"client_id"`               // ID интеграции
	ClientSecret string `json:"client_secret"`           // Секрет интеграции
	GrantType    string `json:"grant_type"`              // Тип авторизационных данных (для кода авторизации – authorization_code)
	Code         string `json:"code,omitempty"`          // Полученный код авторизации
	RefreshCode  string `json:"refresh_token,omitempty"` //? без понятия, почему не кидать рефреш токен в code, но вот так
	RedirectURI  string `json:"redirect_uri"`            // Redirect URI указанный в настройках интеграции
}

// ама выдает не то, что необходимо: expiry тут является expires_in
type oauthTokenForm struct {
	TokenType    string `json:"token_type"`
	ExpiresIn    int    `json:"expires_in"`
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

func (c *ClientBase) ProcessAuthCode(referer, authCode string) (*oauth2.Token, error) {
	return c.getNewToken(referer, authCode, false)
}

func (c *ClientBase) RefreshToken(referer, refreshCode string) (*oauth2.Token, error) {
	return c.getNewToken(referer, refreshCode, true)
}

func (c *ClientBase) getNewToken(referer, code string, isRefresh bool) (*oauth2.Token, error) {
	if !strings.Contains(referer, ".") { // так проще чекнуть, что это хост
		return nil, errors.New("referer is not host")
	}

	link, _ := url.Parse(OauthEndpoint.TokenURL)
	link.Host = referer

	var data *accessTokenPost
	if isRefresh {
		data = &accessTokenPost{
			ClientID:     c.ClientID,
			ClientSecret: c.ClientSecret,
			GrantType:    "refresh_token",
			RefreshCode:  code,
			RedirectURI:  c.RedirectURI,
		}
	} else {
		data = &accessTokenPost{
			ClientID:     c.ClientID,
			ClientSecret: c.ClientSecret,
			GrantType:    "authorization_code",
			Code:         code,
			RedirectURI:  c.RedirectURI,
		}
	}

	byteData, _ := json.Marshal(data)
	req, _ := http.NewRequest("POST", link.String(), bytes.NewBuffer(byteData))
	req.Header.Set("Content-Type", "application/json")
	pp.Println(string(byteData))
	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "requesting")
	}

	defer resp.Body.Close()
	byteData, _ = ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {

		pp.Println(string(byteData))
		errData := &ErrorResponse{}
		err = json.Unmarshal(byteData, errData)
		if err != nil {
			return nil, errors.Wrap(err, "can't even decode error response")
		}

		return nil, errData
	}

	token := &oauth2.Token{}
	pre := &oauthTokenForm{}
	err = json.Unmarshal(byteData, pre)
	if err != nil {
		return nil, errors.Wrap(err, "decoding server response")
	}

	token.AccessToken = pre.AccessToken
	token.TokenType = pre.TokenType
	token.RefreshToken = pre.RefreshToken
	token.Expiry = time.Now().Add(time.Second * time.Duration(pre.ExpiresIn))

	return token, nil
}

func (c *ClientBase) makeOauthConfig() oauth2.Config {
	return oauth2.Config{
		ClientID:     c.ClientID,
		ClientSecret: c.ClientSecret,
		Endpoint:     OauthEndpoint,
		RedirectURL:  c.RedirectURI,
		Scopes:       nil, // скоупы указываются при регистрации приложения
	}
}
