package amocrm

import (
	"encoding/json"
	"strings"
	"time"

	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"github.com/tidwall/gjson"
	"github.com/xelaj/go-dry"
)

//==========================================================================================================//
//                                               CSV Parsing                                                //
//==========================================================================================================//

func (l *Lead) MarshalMap() (map[string]string, error) {
	fields := map[string]interface{}{
		"id":    l.ID,
		"name":  l.Name,
		"price": l.Price,
		//"first_name":          "", // deprecated
		//"last_name":           "", // deprecated
		"responsible_user_id": l.ResponsibleUser,
		"created_by":          l.CreatedBy,
		"created_at":          l.CreatedAt,
		"updated_at":          l.UpdatedAt,
		"updated_by":          l.UpdatedBy,
		//"company":             "", //deprecated
		"account_id": l.AccountID,
		"group_id":   l.GroupID,
		//"leads":               "",
		"tags": l.Tags,
	}
	for k, field := range l.Fields {
		fields[customFieldCode+":"+k] = field
	}

	res := make(map[string]string)
	for key, elem := range fields {
		if dry.ReflectIsInteger(elem) {
			// если это нулевое число, пропускаем
			if dry.ConvertInt(elem) == 0 {
				continue
			}
		}
		if t, ok := elem.(time.Time); ok {
			// если это нулевое время, пропускаем
			if t.IsZero() {
				continue
			}
		}

		converted, err := MarshalText(elem)
		if err != nil {
			return nil, errors.Wrap(err, key)
		}
		if converted != "" {
			res[key] = converted
		}
	}

	return res, nil
}

func (l *Lead) UnmarshalMap(in map[string]string) error {
	var err error
	m := make(map[string]interface{})
	for k, v := range in {
		// поскольку mapstructure умеет преобразовывать типы неявно, можем смело перекопировать
		// мап из строк в интерфейс, нам останется только обработать те значения, которые не может
		// нормально обработать mapstructure
		v = strings.Trim(v, " \t\r\n")
		m[k] = v

		// зачем нам тут пустые значения?
		if v == "" {
			delete(m, k)
		}
	}
	delete(m, "") // для гарантии

	if val, ok := in["tags"]; ok {
		m["tags"] = StringToList(val)
	}

	customFields := make(map[string]Field)
	for k, v := range in {
		if !strings.HasPrefix(k, customFieldCode+":") || v == "" {
			continue
		}

		customFieldName := strings.TrimPrefix(k, customFieldCode+":")

		field, err := l.client.UnmarshalField("lead", customFieldName, v)
		if err != nil {
			return errors.Wrap(err, "parsing '"+k+"'")
		}

		customFields[customFieldName] = field

		delete(m, k)
	}
	m[customFieldCode] = customFields

	decoder, _ := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		DecodeHook:  StringToTimeHookFunc(TimeFormatInSheets, StringToSlice(nil)),
		ErrorUnused: true,
		Result:      l,
		TagName:     "csv",
	})
	err = decoder.Decode(m)
	if err != nil {
		return err
	}

	return nil
}

//==========================================================================================================//
//                                               JSON Parsing                                               //
//==========================================================================================================//

type leadJSON struct {
	ID                     int             `json:"id"`                         // ID сделки
	Name                   string          `json:"name"`                       // Название сделки
	Price                  int             `json:"price"`                      // Бюджет сделки
	ResponsibleUserID      int             `json:"responsible_user_id"`        // ID пользователя, ответственного за сделку
	GroupID                int             `json:"group_id"`                   // ID группы, в которой состоит ответственны пользователь за сделку
	StatusID               int             `json:"status_id"`                  // ID статуса, в который добавляется сделка, по-умолчанию – первый этап главной воронки
	PipelineID             int             `json:"pipeline_id"`                // ID воронки, в которую добавляется сделка
	LossReasonID           int             `json:"loss_reason_id"`             // ID причины отказа
	SourceID               int             `json:"source_id"`                  // Требуется GET параметр with. ID источника сделки
	CreatedBy              int             `json:"created_by"`                 // ID пользователя, создающий сделку
	UpdatedBy              int             `json:"updated_by"`                 // ID пользователя, изменяющий сделку
	ClosedAt               int             `json:"closed_at"`                  // Дата закрытия сделки, передается в Unix Timestamp
	CreatedAt              int             `json:"created_at"`                 // Дата создания сделки, передается в Unix Timestamp
	UpdatedAt              int             `json:"updated_at"`                 // Дата изменения сделки, передается в Unix Timestamp
	ClosestTaskAt          int             `json:"closest_task_at"`            // Дата ближайшей задачи к выполнению, передается в Unix Timestamp
	IsDeleted              bool            `json:"is_deleted"`                 // Удалена ли сделка
	CustomFieldsValues     json.RawMessage `json:"custom_fields_values"`       // Массив, содержащий информацию по значениям дополнительных полей, заданных для данной сделки, обрабатываются отдельно, т.к. очень много сложных действий
	Score                  int             `json:"score"`                      // Скоринг сделки
	AccountID              int             `json:"account_id"`                 // ID аккаунта, в котором находится сделка
	IsPriceModifiedByRobot bool            `json:"is_price_modified_by_robot"` // Требуется GET параметр with. Изменен ли в последний раз бюджет сделки роботом
	Embedded               *struct {
		Tags []*struct {
			ID   int    `json:"id"`   // ID тега
			Name string `json:"name"` // Название тега
		} `json:"tags"` // Данные тегов, привязанных к сделке
		LossReason []*struct {
			ID   int    `json:"id"`   // ID причины отказа
			Name string `json:"name"` // Название причины отказа
		} `json:"loss_reason"` // Требуется GET параметр with. Причина отказа сделки
		Contacts []*struct {
			ID     int  `json:"id"`      // ID контакта, привязанного к сделке
			IsMain bool `json:"is_main"` // Является ли контакт главным для сделки
		} `json:"contacts"` // Требуется GET параметр with. Данные контактов, привязанных к сделке
		Companies []*struct {
			ID int `json:"id"` // ID контакта, привязанного к сделке
		} `json:"companies"` // Данные компании, привязанной к сделке, в данном массиве всегда 1 элемент, так как у сделки может быть только 1 компания
		CatalogElements []*struct {
			ID        int         `json:"id"`         // ID элемента, привязанного к сделке
			Metadata  interface{} `json:"metadata"`   // Является ли контакт главным для сделки
			Quantity  int         `json:"quantity"`   // Количество элементов у сделки
			CatalogID int         `json:"catalog_id"` // ID списка, в котором находится элемент
		} `json:"catalog_elements"` // Требуется GET параметр with. Данные элементов списков, привязанных к сделке
	} `json:"_embedded"` // Данные вложенных сущностей
}

func (l *Lead) UnmarshalJSON(b []byte) error {
	m := new(leadJSON)

	metadata := gjson.Parse(string(b))
	err := json.Unmarshal(b, m)
	if err != nil {
		return err
	}

	l.ID = m.ID
	l.Name = m.Name
	l.Price = m.Price
	l.ResponsibleUser = m.ResponsibleUserID
	l.CreatedBy = m.CreatedBy
	l.CreatedAt = time.Unix(int64(m.CreatedAt), 0)
	l.UpdatedBy = m.UpdatedBy
	l.UpdatedAt = time.Unix(int64(m.UpdatedAt), 0)
	l.AccountID = m.AccountID
	l.IsDeleted = m.IsDeleted
	l.GroupID = m.GroupID
	if metadata.Get("closed_at").Value() != nil {
		l.ClosedAt = time.Unix(int64(m.ClosedAt), 0)
	}
	if metadata.Get("closest_task_at").Value() != nil {
		l.ClosestTaskAt = time.Unix(int64(m.ClosestTaskAt), 0)
	}
	if m.Embedded != nil {
		if len(m.Embedded.Tags) != 0 {
			l.Tags = make([]string, len(m.Embedded.Tags))
			for i, tag := range m.Embedded.Tags {
				l.Tags[i] = tag.Name
			}
		}
	}
	l.Fields, err = unmarshalCustomFields(m.CustomFieldsValues)
	if err != nil {
		return errors.Wrap(err, "custom_fields_values")
	}

	return nil
}
