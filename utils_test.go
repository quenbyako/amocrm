package amocrm

import (
	"fmt"
	"testing"

	"github.com/k0kubun/pp"

	"github.com/stretchr/testify/assert"
)

func TestFieldsParsing(t *testing.T) {
	cases := [][]string{
		{"a", "b", "c", "d"},
		{"серия, номер; 123455б,\"\",,", "а это строчка без запятых\nно с новой строкой"},
		{},
	}

	for _, tt := range cases {
		t.Run("", func(t *testing.T) {
			str := ListToString(tt)
			res := StringToList(str)
			assert.Equal(t, tt, res)
		})
	}
}

var (
	abcdField  = TextField("abcd")
	buzzField  = TextField("buzz")
	oofField   = TextField("oof")
	falseField = CheckboxField(false)
	trueField  = CheckboxField(true)
)

func TestMerge(t *testing.T) {
	tests := []struct {
		a           interface{}
		b           interface{}
		wantDirect  interface{}
		wantReverse interface{}
		wantErr     assert.ErrorAssertionFunc
	}{
		{ // 00
			a: &Lead{
				ID:   1488,
				Tags: []string{"a"},
				Fields: map[string]Field{
					"somefield": &abcdField,
					"foo":       &trueField,
					"bar":       &buzzField,
					"buzz":      &oofField,
				},
			},
			b: &Lead{
				ID:   1489,
				Tags: []string{"b"},
				Fields: map[string]Field{
					"somefield": &abcdField,
					"foo":       &trueField,
					"bar":       &buzzField,
					"buzz":      &oofField,
				},
			},
			wantDirect: &Lead{
				ID:   1489,
				Tags: []string{"a", "b"},
				Fields: map[string]Field{
					"somefield": &abcdField,
					"foo":       &trueField,
					"bar":       &buzzField,
					"buzz":      &oofField,
				},
			},
			wantReverse: &Lead{
				ID:   1489,
				Tags: []string{"b"},
				Fields: map[string]Field{
					"somefield": &abcdField,
					"foo":       &trueField,
					"bar":       &buzzField,
					"buzz":      &oofField,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run("", func(t *testing.T) {
			if tt.wantErr == nil {
				tt.wantErr = assert.NoError
			}

			gotDirect, err := Merge(tt.a, tt.b, false)
			if !tt.wantErr(t, err) {
				fmt.Println(err)
				return
			}
			if !assert.Equal(t, tt.wantDirect, gotDirect) {
				pp.Println("direct error")
			}

			gotReverse, err := Merge(tt.a, tt.b, true)
			if !tt.wantErr(t, err) {
				fmt.Println(err)
				return
			}
			if !assert.Equal(t, tt.wantReverse, gotReverse) {
				pp.Println("reverse error")
			}
		})
	}
}

func TestMergeSingleItem(t *testing.T) {
	tests := []struct {
		a           interface{}
		b           interface{}
		wantDirect  interface{}
		wantReverse interface{}
		wantErr     assert.ErrorAssertionFunc
	}{
		{ // 00
			a:           123,
			b:           456,
			wantDirect:  456,
			wantReverse: 456,
		},
		{ // 01
			a:           123,
			b:           0,
			wantDirect:  123,
			wantReverse: 0,
		},
		{ // 02
			a:           0,
			b:           456,
			wantDirect:  456,
			wantReverse: 456,
		},
		{ // 03
			a:           0,
			b:           0,
			wantDirect:  0,
			wantReverse: 0,
		},
		{ // 04
			a:           map[string]int{},
			b:           map[string]int{"a": 1},
			wantDirect:  map[string]int{"a": 1},
			wantReverse: map[string]int{"a": 1},
		},
		{ // 05
			a:           map[string]int{"b": 1},
			b:           nil,
			wantDirect:  map[string]int{"b": 1},
			wantReverse: map[string]int{},
		},
		{ // 06
			a:           map[string]int{"b": 1},
			b:           map[string]int{"b": 2},
			wantDirect:  map[string]int{"b": 2},
			wantReverse: map[string]int{"b": 2},
		},
		{ // 07
			a:           map[string]int{"b": 1},
			b:           map[string]int{"a": 1},
			wantDirect:  map[string]int{"b": 1, "a": 1},
			wantReverse: map[string]int{"a": 1},
		},
		{ // 08
			a:           map[string]int{"b": 1},
			b:           map[string]int{"a": 1, "b": 2},
			wantDirect:  map[string]int{"a": 1, "b": 2},
			wantReverse: map[string]int{"a": 1, "b": 2},
		},
		{ // 09
			a:           map[string]int{"a": 1},
			b:           map[string]int{"a": 1, "b": 1},
			wantDirect:  map[string]int{"a": 1, "b": 1},
			wantReverse: map[string]int{"a": 1, "b": 1},
		},
		{ // 10
			a:           map[string]int{"a": 1},
			b:           map[string]int{"a": 2},
			wantDirect:  map[string]int{"a": 2},
			wantReverse: map[string]int{"a": 2},
		},
		{ // 11
			a:           map[string]int{"a": 1},
			b:           map[string]int{"a": 2, "b": 1},
			wantDirect:  map[string]int{"a": 2, "b": 1},
			wantReverse: map[string]int{"a": 2, "b": 1},
		},
		{ // 12
			a:           map[string]int{"a": 1, "b": 1},
			b:           map[string]int{"b": 1},
			wantDirect:  map[string]int{"a": 1, "b": 1},
			wantReverse: map[string]int{"b": 1},
		},
		{ // 13
			a:           map[string]int{"a": 1, "b": 1},
			b:           map[string]int{"b": 2},
			wantDirect:  map[string]int{"a": 1, "b": 2},
			wantReverse: map[string]int{"b": 2},
		},
		{ // 14
			a:           map[string]int{"a": 1, "b": 1},
			b:           map[string]int{"a": 1},
			wantDirect:  map[string]int{"a": 1, "b": 1},
			wantReverse: map[string]int{"a": 1},
		},
		{ // 15
			a:           map[string]int{"a": 1, "b": 1},
			b:           map[string]int{"a": 1, "b": 2},
			wantDirect:  map[string]int{"a": 1, "b": 2},
			wantReverse: map[string]int{"a": 1, "b": 2},
		},
		{ // 16
			a:           map[string]int{"a": 1, "b": 1},
			b:           map[string]int{"a": 2},
			wantDirect:  map[string]int{"a": 2, "b": 1},
			wantReverse: map[string]int{"a": 2},
		},
		{ // 17
			a:           map[string]int{"a": 1, "b": 1},
			b:           map[string]int{"a": 2, "b": 1},
			wantDirect:  map[string]int{"a": 2, "b": 1},
			wantReverse: map[string]int{"a": 2, "b": 1},
		},
		{ // 18
			a:           map[string]int{"a": 1, "b": 1},
			b:           map[string]int{"a": 2, "b": 2},
			wantDirect:  map[string]int{"a": 2, "b": 2},
			wantReverse: map[string]int{"a": 2, "b": 2},
		},
		{ // 19
			a:           []string{},
			b:           []string{},
			wantDirect:  []string{},
			wantReverse: []string{},
		},
		{ // 20
			a:           []string{},
			b:           []string{"a"},
			wantDirect:  []string{"a"},
			wantReverse: []string{"a"},
		},
		{ // 21
			a:           []string{"a"},
			b:           []string{},
			wantDirect:  []string{"a"},
			wantReverse: []string{},
		},
		{ // 22
			a:           []string{"a"},
			b:           []string{"a"},
			wantDirect:  []string{"a"},
			wantReverse: []string{"a"},
		},
		{ // 23
			a:           []string{"a"},
			b:           []string{"b"},
			wantDirect:  []string{"a", "b"},
			wantReverse: []string{"b"},
		},
		{ // 24
			a:           []string{"a"},
			b:           []string{"a", "b"},
			wantDirect:  []string{"a", "b"},
			wantReverse: []string{"a", "b"},
		},
		{ // 25
			a:           []string{"b"},
			b:           []string{"a"},
			wantDirect:  []string{"a", "b"},
			wantReverse: []string{"a"},
		},
		{ // 26
			a:           []string{"a", "b"},
			b:           []string{"a"},
			wantDirect:  []string{"a", "b"},
			wantReverse: []string{"a"},
		},
	}
	for _, tt := range tests {
		t.Run("", func(t *testing.T) {
			if tt.wantErr == nil {
				tt.wantErr = assert.NoError
			}

			gotDirect, err := mergeSingleField(tt.a, tt.b, false)
			tt.wantErr(t, err)
			if !assert.Equal(t, tt.wantDirect, gotDirect) {
				pp.Println("direct error")
			}

			gotReverse, err := mergeSingleField(tt.a, tt.b, true)
			tt.wantErr(t, err)
			if !assert.Equal(t, tt.wantReverse, gotReverse) {
				pp.Println("reverse error")
			}
		})
	}
}
