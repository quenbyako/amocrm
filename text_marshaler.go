package amocrm

import (
	"bytes"
	"encoding"
	"encoding/csv"
	"fmt"
	"reflect"
	"strconv"
	"time"

	"github.com/pkg/errors"
	"github.com/xelaj/go-dry"
)

func MarshalText(in interface{}) (string, error) {
	switch val := in.(type) {
	case string:
		return val, nil
	case []byte:
		return string(val), nil
	case bool:
		return strconv.FormatBool(val), nil
	case int8, uint8, int16, uint16, int32, uint32, int64, uint64, int, uint:
		return strconv.Itoa(dry.ConvertInt(val)), nil
	case float32, float64:
		return strconv.FormatFloat(dry.ConvertFloat(val), 'f', -1, 64), nil
	case time.Time:
		return val.Format(TimeFormatInSheets), nil
	}

	inval := reflect.ValueOf(in)
	intyp := inval.Type()

	unmarshaler := reflect.TypeOf((*encoding.TextMarshaler)(nil)).Elem()
	if intyp.Implements(unmarshaler) {
		v := inval.Convert(unmarshaler).Interface().(encoding.TextMarshaler)
		str, err := v.MarshalText()
		if err != nil {
			return "", err
		}
		return string(str), nil
	}

	stringer := reflect.TypeOf((*fmt.Stringer)(nil)).Elem()
	if intyp.Implements(stringer) {
		v := inval.Convert(stringer).Interface().(fmt.Stringer)
		return v.String(), nil
	}

	if intyp.Kind() == reflect.Slice {
		var str string
		var err error
		stringed := make([]string, 0)
		dry.SliceForEach(in, func(index int, item interface{}) {
			// don't do anything if err was found
			if err != nil {
				return
			}
			str, err = MarshalText(item)
			if err != nil {
				err = errors.Wrap(err, "["+strconv.Itoa(index)+"]")
				return
			}
			stringed = append(stringed, str)
		})
		if err != nil {
			return "", err // err cause we already wrapped it
		}

		buf := bytes.NewBufferString("")
		w := csv.NewWriter(buf)
		w.Comma = ';'
		err = w.Write(stringed)
		dry.PanicIfErr(err)
		w.Flush()
		res := buf.String()
		res = res[:len(res)-1] // удаляем последний \n
		return res, nil
	}

	return "", errors.New("type not supported: " + intyp.String())
}

type decodeHook func(in interface{}) (interface{}, error)

var decodeHooks = map[reflect.Type]decodeHook{
	reflect.TypeOf(time.Time{}): nil,
}
