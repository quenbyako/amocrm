package amocrm

import (
	"testing"

	"github.com/k0kubun/pp"
	"github.com/stretchr/testify/assert"
)

/*
func TestUpdateContact(t *testing.T) {
	return
	prepareClient(t)

	contact, err := client.GetContactsByID(42908659)
	assert.NoError(t, err)
	pp.Println(contact)

	contact[0].FirstName = "Вася"
	contact[0].Fields["Телефон"] = []string{"12345"}
	contact[0].Fields["Должность"] = nil

	err = client.UpdateContact(contact[0])
	assert.NoError(t, err)
}
*/

func TestGetAllContacts(t *testing.T) {
	tests := []struct {
		limit   int
		with    []WithParameter
		wantErr assert.ErrorAssertionFunc
	}{
		{
			-1,
			[]WithParameter{
				WithCatalogElements,
				WithIsPriceModifiedByRobot,
				WithLossReason,
				WithContacts,
				WithSourceID,
			},
			assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run("", func(t *testing.T) {
			a, err := client.GetAllContacts(tt.limit, tt.with)
			tt.wantErr(t, err)
			pp.Println(a)
		})
	}
}
