package amocrm

import (
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"github.com/xelaj/go-dry"

	"github.com/mitchellh/mapstructure"

	"github.com/pkg/errors"
	"golang.org/x/text/currency"
)


func parseUsersInfo(b *json.RawMessage) ([]*UserInfo, error) {
	rawMessage := &map[int]struct {
		Id          int    `json:"id"`
		Name        string `json:"name"`
		Login       string `json:"login"`
		Language    string `json:"language"`
		PhoneNumber string `json:"phone_number"`
		GroupId     int    `json:"group_id"`
		IsActive    bool   `json:"is_active"`
		IsFree      bool   `json:"is_free"`
		IsAdmin     bool   `json:"is_admin"`
		//Rights    interface{} `json:"rights"` // не спрашивай почему так. by_status портит весь map[string]string
	}{}

	err := json.Unmarshal(*b, rawMessage)
	if err != nil {
		return nil, err
	}

	res := make([]*UserInfo, len(*rawMessage))
	i := 0
	for _, item := range *rawMessage {
		//rights := map[string]UserPermission{}

		res[i] = &UserInfo{
			ID:          item.Id,
			Name:        item.Name,
			Login:       item.Login,
			Language:    item.Language,
			PhoneNumber: item.PhoneNumber,
			GroupID:     item.GroupId,
			IsActive:    item.IsActive,
			IsFree:      item.IsFree,
			IsAdmin:     item.IsAdmin,
			//Rights:      rights,
		}

		i++
	}
	return res, nil
}

func parseDatePatterns(in map[string]string) map[string]string {
	for k, v := range in {
		in[k] = AmoDateFormat(v)
	}
	return in
}

func parseCustomFieldsDescription(b *json.RawMessage) (map[string][]*CustomFieldDescription, error) {
	rawMessage := &map[string]*json.RawMessage{}
	//grouppedFields := map[string][]fieldRawMessage{}

	err := json.Unmarshal(*b, rawMessage)
	if err != nil {
		fmt.Println(string(*b))
		return nil, err
	}

	// вычищаем список от дерьмовых элементов, типа пустых списков, мапов с ключом в виде айдишника и т.д.
	fieldsInterfaces := make(map[string][]interface{})
	for key, rawjson := range *rawMessage {
		var in interface{}
		err = json.Unmarshal(*rawjson, &in)
		if err != nil {
			return nil, err
		}
		switch item := in.(type) {
		case []interface{}:
			continue // пусто
		case map[string]interface{}:
			res := make([]interface{}, len(item))
			i := 0
			for _, element := range item {
				res[i] = element
				i++
			}
			fieldsInterfaces[key] = res
		}
	}

	res := make(map[string][]*CustomFieldDescription)
	for part, fields := range fieldsInterfaces {
		parsedFields := make([]*CustomFieldDescription, len(fields))
		for i, field := range fields {
			decodedField := new(fieldRawMessage)
			d, _ := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
				ErrorUnused: false, // true,
				Result:      decodedField,
				TagName:     "json",
			})
			err := d.Decode(field)
			if err != nil {
				return nil, err
			}

			parsedFields[i] = &CustomFieldDescription{
				ID:         decodedField.ID,
				Name:       decodedField.Name,
				FieldType:  CustomFieldType(decodedField.FieldType),
				Sort:       decodedField.Sort,
				IsMultiple: decodedField.IsMultiple,
				IsSystem:   decodedField.IsSystem,
				IsEditable: decodedField.IsEditable,
				Enums:      normalizeEnums(decodedField.Enums),
			}
		}
		res[part] = parsedFields
	}
	return res, nil
}

func parsePipelines(b *json.RawMessage) ([]*Pipeline, error) {
	rawMessage := map[int]struct {
		ID       int            `json:"id"`
		Name     string         `json:"name"`
		Sort     int            `json:"sort"`
		IsMain   bool           `json:"is_main"`
		Statuses map[int]Status `json:"statuses"`
	}{}

	err := json.Unmarshal(*b, &rawMessage)
	if err != nil {
		fmt.Println(string(*b))
		return nil, err
	}

	res := make([]*Pipeline, len(rawMessage))
	i := 0
	for _, item := range rawMessage {
		pipeline := &Pipeline{
			ID:       item.ID,
			Name:     item.Name,
			Sort:     item.Sort,
			IsMain:   item.IsMain,
			Statuses: normalizeStatuses(item.Statuses),
		}

		res[i] = pipeline
		i++
	}

	return res, nil
}

func normalizeStatuses(raw map[int]Status) []*Status {
	res := make([]*Status, len(raw))
	i := 0
	for _, item := range raw {
		s := &Status{
			ID:       item.ID,
			Name:     item.Name,
			Sort:     item.Sort,
			Color:    item.Color,
			Editable: item.Editable,
		}
		res[i] = s
		i++
	}
	return res
}

type fieldRawMessage struct {
	ID         int               `json:"id"`
	Name       string            `json:"name"`
	FieldType  int               `json:"field_type"`
	Sort       int               `json:"sort"`
	IsMultiple bool              `json:"is_multiple"`
	IsSystem   bool              `json:"is_system"`
	IsEditable bool              `json:"is_editable"`
	Enums      map[string]string `json:"enums"`
}

func normalizeEnums(in map[string]string) map[int]string {
	res := make(map[int]string)
	for k, v := range in {
		id, err := strconv.Atoi(k)
		dry.PanicIfErr(err)
		res[id] = v
	}
	return res
}
