package amocrm

import (
	"context"
	"encoding/json"
	"fmt"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/xelaj/go-dry"

	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
)

type Interval struct {
	From time.Time
	To   time.Time
}

type EventFilter struct {
	//id           []string // MAX 100
	//created_at   Interval
	//created_by   []int    // MAX 10
	//entity       []string // возможные значения lead, contact, company, customer, task, catalog_{CATALOG_ID}
	//entity_id    []int    // MAX 10
	Type []string
	//value_before string
	//value_after  string
}

func (f *EventFilter) Values() *url.Values {
	v := url.Values{}

	if f == nil {
		return &v
	}

	if f.Type != nil {
		v["filter[type]"] = []string{strings.Join(f.Type, ",")}
	}

	return &v
}

type EventEntity struct {
	Type string
	ID   string // could be int for some reason, see docs (https://www.amocrm.ru/developers/content/api/events)
	Name string
}

func (e *EventEntity) UnmarshalJSON(b []byte) error {
	m := make(map[string]interface{})

	err := json.Unmarshal(b, &m)
	if err != nil {
		return err
	}

	if e == nil {
		e = new(EventEntity)
	}

	e.Type = m["type"].(string)
	if v, ok := m["name"]; ok {
		e.Name = v.(string)
	}
	switch m["id"].(type) {
	case string:
		e.ID = m["id"].(string)
	case float64:
		e.ID = strconv.Itoa(int(m["id"].(float64)))
	default:
		panic("unknown type in response")
	}

	return nil
}

//==========================================================================================================//

type Event struct {
	ID          string        `json:"id"`           // ID события
	Type        string        `json:"type"`         // Тип события
	EntityID    int           `json:"entity_id"`    // ID сущности события (id лида, id контакта и прочее)
	EntityType  string        `json:"entity_type"`  // Сущность события
	CreatedBy   int           `json:"created_by"`   // ID пользователя, создавший событие
	CreatedAt   time.Time     `json:"created_at"`   // Дата создания события, передается в Unix Timestamp
	ValueAfter  []interface{} `json:"value_after"`  // Массив с изменениями по событию.
	ValueBefore []interface{} `json:"value_before"` // Массив с изменениями по событию.
	AccountID   int           `json:"account_id"`   // ID аккаунта, в котором находится событие
}

func (e *Event) UnmarshalJSON(b []byte) error {
	m := make(map[string]interface{})

	err := json.Unmarshal(b, &m)
	if err != nil {
		return err
	}

	if val, ok := m["created_at"]; ok {
		v, ok := val.(float64)
		if !ok {
			return errors.New("'created_at' must be number")
		}
		m["created_at"] = time.Unix(int64(v), 0)
	}

	delete(m, "_links")
	delete(m, "_embedded")

	d, _ := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		ErrorUnused: true,
		Result:      e,
		TagName:     "json",
	})
	err = d.Decode(m)
	if err != nil {
		return err
	}

	return nil
}

type EventWithParam string

func (p EventWithParam) String() string {
	return string(p)
}

const (
	WithContactName        EventWithParam = "contact_name"         // Если сущностью события является контакт, то помимо его ID, вы получите и название
	WithLeadName           EventWithParam = "lead_name"            // Если сущностью события является сделка, то помимо её ID, вы получите и название
	WithCompanyName        EventWithParam = "company_name"         // Если сущностью события является компания, то помимо её ID, вы получите и название
	WithCatalogElementName EventWithParam = "catalog_element_name" // Если сущностью события является элемент каталога, то помимо его ID, вы получите и название
	WithCustomerName       EventWithParam = "customer_name"        // Если сущностью события является покупатель, то помимо его ID, вы получите и название
	WithCatalogName        EventWithParam = "catalog_name"         // Если сущностью события является элемент каталога, то помимо ID каталога, к которому он относится, вы получите и название каталога
)

type GetLastEventsParams struct {
	With  []EventWithParam
	Page  int
	Limit int
	// Filter
}

func (p *GetLastEventsParams) toValues() url.Values {
	v := make(url.Values)
	if len(p.With) != 0 {
		elements := make([]fmt.Stringer, len(p.With))
		for i, item := range p.With {
			elements[i] = fmt.Stringer(item)
		}
		v.Set("with", stringifyWithParams(elements...))
	}
	if p.Page != 0 {
		v.Set("page", strconv.Itoa(p.Page))
	}
	if p.Limit != 0 {
		v.Set("limit", strconv.Itoa(p.Limit))
	}

	return v
}

type eventsResponse struct {
	Embedded struct {
		Events []*Event `json:"events"`
	} `json:"_embedded"`
}

func (c *ClientSpecific) GetLastEvents(params GetLastEventsParams) ([]*Event, error) {
	response := new(eventsResponse)
	err := c.Request("GET", "/events", params.toValues(), response, nil)
	if err != nil {
		return nil, err
	}

	return response.Embedded.Events, nil
}

//==========================================================================================================//

/*
 * lead_added                    Новая сделка
 * lead_deleted                  Сделка удалена
 * lead_restored                 Сделка восстановлена
 * lead_status_changed           Изменение этапа продажи
 * lead_linked                   Прикрепление сделки
 * lead_unlinked                 Открепление сделки
 * contact_added                 Новый контакт
 * contact_deleted               Контакт удален
 * contact_restored              Контакт восстановлен
 * contact_linked                Прикрепление контакта
 * contact_unlinked              Открепление контакта
 * company_added                 Новая компания
 * company_deleted               Компания удалена
 * company_restored              Компания восстановлена
 * company_linked                Прикрепление компании
 * company_unlinked              Открепление компании
 * customer_added                Новый покупатель
 * customer_deleted              Покупатель удален
 * customer_status_changed       Изменение этапа покупателя
 * customer_linked               Прикрепление покупателя
 * customer_unlinked             Открепление покупателя
 * task_added                    Новая задача
 * task_deleted                  Задача удалена
 * task_completed                Завершение задачи
 * task_type_changed             Изменение типа задачи
 * task_text_changed             Изменение текста задачи
 * task_deadline_changed         Изменение даты исполнения задачи
 * task_result_added             Результат по задаче
 * incoming_call                 Входящий звонок
 * outgoing_call                 Исходящий звонок
 * incoming_chat_message         Входящее сообщение
 * outgoing_chat_message         Исходящее сообщение
 * incoming_sms                  Входящее SMS
 * outgoing_sms                  Исходящее SMS
 * entity_tag_added              Теги добавлены
 * entity_tag_deleted            Теги убраны
 * entity_linked                 Прикрепление
 * entity_unlinked               Открепление
 * sale_field_changed            Изменение поля “Бюджет”
 * name_field_changed            Изменение поля “Название”
 * ltv_field_changed             Сумма покупок
 * custom_field_value_changed    Изменение поля
 * entity_responsible_changed    Ответственный изменен
 * robot_replied                 Ответ робота
 * intent_identified             Тема вопроса определена
 * nps_rate_added                Новая оценка NPS
 * link_followed                 Переход по ссылке
 * transaction_added             Добавлена покупка
 * common_note_added             Новое примечание
 * common_note_deleted           Примечание удалено
 * attachment_note_added         Добавлен новый файл
 * targeting_in_note_added       Добавление в ретаргетинг
 * targeting_out_note_added      Удаление из ретаргетинга
 * geo_note_added                Новое примечание с гео-меткой
 * service_note_added            Новое системное примечание
 * site_visit_note_added         Заход на сайт
 * message_to_cashier_note_added LifePay: Сообщение кассиру
 * entity_merged                 Выполнено объединение
 * custom_field_{}_value_changed Изменение поля c переданным ID. Если вы передаете данный тип, то другие типы не могут быть переданы.
 *
 * кастомные типы обновлений:
 *
 * lead_changed                  изменение лида (любой эвент с энтити lead)
 * contact_changed               изменение контакта (любой эвент с энтити contact)

 */

type OnEventFunc func(e *Event) error

type EventListener struct {
	onEvents              map[string]OnEventFunc
	onSpecificEvents      map[string]OnEventFunc
	Client                *ClientSpecific
	Timeout               time.Duration
	lastEventProceedSince time.Time
}

func (c *ClientSpecific) NewEventListener() *EventListener {
	return &EventListener{
		onEvents:         make(map[string]OnEventFunc),
		onSpecificEvents: make(map[string]OnEventFunc),
		Client:           c,
		Timeout:          2 * time.Second,
	}
}

func (e *EventListener) On(event string, f OnEventFunc) {
	if dry.SliceContains([]string{
		"lead_changed",
		"contact_changed",
	}, event) {
		e.onSpecificEvents[event] = f
	}

	e.onEvents[event] = f

}

func (e *EventListener) Start(ctx context.Context) chan error {
	e.lastEventProceedSince = time.Now()

	errChan := make(chan error)
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			default:
				events, err := e.Client.GetLastEvents(GetLastEventsParams{
					With: []EventWithParam{
						WithContactName,
						WithLeadName,
						WithCompanyName,
						WithCatalogElementName,
						WithCustomerName,
						WithCatalogName,
					},
					Limit: 100,
				})
				if err != nil {
					errChan <- err
				}

				usefulEvents := make([]*Event, 0)
				for _, event := range events {
					if event.CreatedAt.After(e.lastEventProceedSince) {
						usefulEvents = append(usefulEvents, event)
					}
				}

				if len(usefulEvents) != 0 {
					e.lastEventProceedSince = time.Now().Round(time.Second)
				}

				for _, event := range usefulEvents {
					err := e.proceedEvent(event)

					if err != nil {
						errChan <- err
					}
				}
				time.Sleep(e.Timeout)
			}
		}
	}()

	return errChan
}

func (e *EventListener) proceedEvent(event *Event) error {
	f, ok := e.onEvents[event.Type]
	if !ok {
		customEventType := ""
		switch event.EntityType {
		case "lead":
			customEventType = "lead_changed"
		case "contact":
			customEventType = "contact_changed"
		default:
			return nil
		}

		f, ok = e.onSpecificEvents[customEventType]
		if !ok {
			return nil
		}
	}

	return f(event)
}
