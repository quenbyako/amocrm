package amocrm

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
	"time"

	"github.com/k0kubun/pp"

	"golang.org/x/oauth2"

	"github.com/xelaj/go-dry"
)

var (
	client                 *ClientSpecific
	OmitContactValuesTests = true
	OmitWebRequestingTests = false
	OmitEventListenerTests = true
)

func tearup() {
	base, err := NewClient(ClientParams{
		ClientID:     "cabf7b8d-6786-4788-805d-46017e0c05d7",
		ClientSecret: "b5QvrYCsl5fEYdyumXRZwxYYIw2FN5QM9hw9jThlwtqRjhUyABYaZahfOeknsst2",
		RedirectURI:  "https://xelaj.org:9999",
	})
	dry.PanicIfErr(err)

	oauthPath := filepath.Join(dry.TestGetCurrentPackagePath(), "testdata", "oauth_token.json")

	data, err := ioutil.ReadFile(oauthPath)
	dry.PanicIfErr(err)

	oauthToken := &oauth2.Token{}
	err = json.Unmarshal(data, oauthToken)
	dry.PanicIfErr(err)

	if oauthToken.Expiry.Before(time.Now()) {
		oauthToken, err = base.RefreshToken("jopap53443.amocrm.ru", oauthToken.RefreshToken)
		dry.PanicIfErr(err)

		data, _ := json.MarshalIndent(oauthToken, "", "    ")
		err := ioutil.WriteFile(oauthPath, data, 0644)
		dry.PanicIfErr(err)

		println("token was refreshed")
	}

	client = base.As("jopap53443", oauthToken)
}

func teardown() {
}

func TestMain(m *testing.M) {
	tearup()
	code := m.Run()
	teardown()
	os.Exit(code)
}

func TestGettingContacts(t *testing.T) {
	pp.Println(client.GetContacts(GetContactsParams{
		With: []WithParameter{
			WithCatalogElements,
			WithLeads,
			WithCustomers,
		},
		Limit: 100,
	}))
}

/*

// точечный тест, оставлю на всякий
func TestGetContactsFields(t *testing.T) {
	prepareClient(t)

	contact, err := client.GetContactsByID(42908659)
	assert.NoError(t, err, "error is not nil")

	pp.Println(contact)
}

func TestGettingAccountPipelines(t *testing.T) {
	if OmitWebRequestingTests {
		fmt.Println("!!! OMIT: TestGettingContactsById...")
		return
	}
	prepareClient(t)
	_, err := client.SyncAccountData()

	assert.NoError(t, err, "error is not nil")
	pp.SetDefaultMaxDepth(-1)
	// pp.Println(client.accountData)
	for _, pipeline := range client.accountData.Pipelines {
		pp.Println(pipeline.Name, pipeline.ID)
		for _, status := range pipeline.Statuses {
			pp.Println(status)
		}
	}
}

func TestGetAccountData(t *testing.T) {
	if OmitWebRequestingTests {
		fmt.Println("!!! OMIT: TestGettingContactsById...")
		return
	}
	prepareClient(t)
	res, err := client.SyncAccountData()
	assert.NoError(t, err)
	pp.Println(res)
}
*/
