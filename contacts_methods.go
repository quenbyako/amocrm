package amocrm

import (
	"encoding/json"
	"fmt"
	"log"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
)

type GetContactsParams struct {
	With  []WithParameter
	Page  int
	Limit int
	Query string
	// Filter
	// Order
}

func (p *GetContactsParams) toValues() url.Values {
	v := make(url.Values)
	if len(p.With) != 0 {
		elements := make([]fmt.Stringer, len(p.With))
		for i, item := range p.With {
			elements[i] = fmt.Stringer(item)
		}
		v.Set("with", stringifyWithParams(elements...))
	}
	if p.Page != 0 {
		v.Set("page", strconv.Itoa(p.Page))
	}
	if p.Limit != 0 {
		v.Set("limit", strconv.Itoa(p.Limit))
	}
	if p.Query != "" {
		v.Set("query", p.Query)
	}

	return v
}

type contactsResponse struct {
	Embedded struct {
		Contacts []*Contact `json:"contacts"`
	} `json:"_embedded"`
	Page  int                          `json:"_page"`
	Links map[string]map[string]string `json:"_links"`
}

func (c *ClientSpecific) GetContacts(params GetContactsParams) ([]*Contact, error) {
	resp := new(contactsResponse)
	err := c.Request("GET", "/contacts", params.toValues(), resp, nil)
	if err != nil {
		return nil, err
	}

	for _, item := range resp.Embedded.Contacts {
		item.client = c
	}

	return resp.Embedded.Contacts, nil
}

type getContactsByID struct {
	With []WithParameter
}

func (p getContactsByID) toValues() url.Values {
	v := make(url.Values)
	if len(p.With) != 0 {
		elements := make([]fmt.Stringer, len(p.With))
		for i, item := range p.With {
			elements[i] = fmt.Stringer(item)
		}
		v.Set("with", stringifyWithParams(elements...))
	}

	return v
}

func (c *ClientSpecific) GetContactByID(id int, with ...WithParameter) (*Contact, error) {
	response := new(Contact)
	err := c.Request("GET", "/contacts/"+strconv.Itoa(id), getContactsByID{With: with}.toValues(), response, nil)
	if err != nil {
		return nil, err
	}

	response.client = c

	return response, nil
}

func (c *ClientSpecific) GetAllContacts(limit int, with []WithParameter) ([]*Contact, error) {
	contactsAll := make([]*Contact, 0)

	for page := 0; ; page++ {
		if debugMode {
			print(".")
		}
		contacts, err := c.GetContacts(GetContactsParams{
			With:  with,
			Page:  page,
			Limit: ContactsMaxLimitPerRequest,
		})
		if err != nil {
			// если нет контента, то отдаем все что нашли, дальше то точно контента не будет
			if _, ok := err.(*ErrorNoContent); ok {
				return contactsAll, nil
			}
			return nil, err
		}

		contactsAll = append(contactsAll, contacts...)

		if limit > 0 && len(contactsAll) >= limit {
			return contactsAll[:limit], nil
		}

		if len(contacts) < ContactsMaxLimitPerRequest {
			break // если он вернул меньше, то последующие зарпросы 100% ничего не вернут
		}
	}

	return contactsAll, nil
}

/*
type postContactRequestBody struct {
	Add    []postContactRequestBodyAdd    `json:"add,omitempty"`
	Update []postContactRequestBodyUpdate `json:"update,omitempty"`
}

type postContactRequestBodyAdd struct {
	Name            string                    `json:"name"` // require
	FirstName       string                    `json:"first_name,omitempty"`
	LastName        string                    `json:"last_name,omitempty"`
	CreatedAt       int                       `json:"created_at,omitempty"` // могут быть string по документации???
	UpdatedAt       int                       `json:"updated_at,omitempty"` // могут быть string по документации???
	ResponsibleUser int                       `json:"responsible_user_id,omitempty"`
	CreatedBy       int                       `json:"created_by,omitempty"`
	CompanyName     string                    `json:"company_name,omitempty"`
	Tags            string                    `json:"tags,omitempty"`
	LeadsID         []int                     `json:"leads_id,omitempty"`
	CompanyID       string                    `json:"company_id,omitempty"`
	CustomersID     string                    `json:"customers_id,omitempty"` // список из айдишников покупателей записаных через запятую ой блять какой же пиздец
	CustomFiels     []customFieldRawRepresent `json:"custom_fields,omitempty"`
}
*/

type updateContactParams struct {
	Name               string          `json:"name,omitempty"`                 // Название контакта
	FirstName          string          `json:"first_name,omitempty"`           // Имя контакта
	LastName           string          `json:"last_name,omitempty"`            // Фамилия контакта
	ResponsibleUser    int             `json:"responsible_user_id,omitempty"`  // ID пользователя, ответственного за контакт
	CreatedBy          int             `json:"created_by,omitempty"`           // ID пользователя, создавший контакт
	UpdatedBy          int             `json:"updated_by,omitempty"`           // ID пользователя, изменивший контакт
	CreatedAt          int             `json:"created_at,omitempty"`           // Дата создания контакта, передается в Unix Timestamp
	UpdatedAt          int64           `json:"updated_at,omitempty"`           // Дата изменения контакта, передается в Unix Timestamp
	CustomFieldsValues json.RawMessage `json:"custom_fields_values,omitempty"` // Массив, содержащий информацию по значениям дополнительных полей, заданных для данного контакта. Примеры заполнения полей
	//_embedded 	object 	Данные вложенных сущностей
	//_embedded[tags] 	array 	Данные тегов, привязанных к контакту
	//_embedded[tags][0] 	object 	Модель тега, привязанного к контакту
	//_embedded[tags][0][id] 	int 	ID тега
	//_embedded[tags][0][name] 	string 	Название тега
}

func (c *ClientSpecific) UpdateContact(contact *Contact) error {
	if contact.ID == 0 {
		return errors.New("контакт необходимо создать прежде чем обновлять (contact_id == 0)")
	}

	if debugMode {
		if !(strings.Contains(contact.Name, "тест") ||
			strings.Contains(contact.FirstName, "тест") ||
			strings.Contains(contact.LastName, "тест")) {
			log.Println("ой, другие контакты пока нельзя менять ради безопасности")
			return errors.New("account is restricted to edit")
		}
	}

	existed, err := c.GetContactByID(contact.ID)
	if err != nil {
		return errors.Wrap(err, "контакт не найден в сервисе")
	}

	var requireToSend bool // если хоть одно поле изменилось, то мы отсылаем апдейт, иначе не пересылаем лишних
	//                        реквестов.
	body := &updateContactParams{
		UpdatedAt: time.Now().Unix(),
	}

	if contact.Name != existed.Name {
		body.Name = contact.Name
		requireToSend = true
	}

	if contact.FirstName != existed.FirstName {
		body.FirstName = contact.FirstName
		requireToSend = true
	}

	if contact.LastName != existed.LastName {
		body.LastName = contact.LastName
		requireToSend = true
	}

	if contact.ResponsibleUser != existed.ResponsibleUser {
		body.ResponsibleUser = contact.ResponsibleUser
		requireToSend = true
	}

	updatedCustomFields := UpdateFields(existed.Fields, contact.Fields)
	if len(updatedCustomFields) > 0 {
		var err error
		body.CustomFieldsValues, err = c.marshalFieldsForUpdates("contact", updatedCustomFields)
		if err != nil {
			return errors.Wrap(err, "marshalling fields")
		}
		requireToSend = true
	}

	if !requireToSend {
		return nil
	}

	data, _ := json.MarshalIndent(body, "", "  ")
	fmt.Println(string(data))
	err = c.Request("PATCH", "/contacts/"+strconv.Itoa(contact.ID), nil, nil, data)
	if err != nil {
		return err
	}

	return nil
}
