package amocrm

import (
	"reflect"
)

// выдает те филды, которые были изменены
// f1 считается старым, f2 новым
func UpdateFields(f1, f2 map[string]Field) map[string]Field {
	res := make(map[string]Field)

	for k, v := range f2 {
		if _, ok := f1[k]; !ok {
			res[k] = v
		}
	}

	for k, v1 := range f1 {
		v2, ok := f2[k]
		if !ok {
			res[k] = reflect.New(reflect.TypeOf(v1).Elem()).Interface().(Field)
		} else {
			if v1.String() != v2.String() {
				res[k] = v2
			}
		}
	}

	return res
}
