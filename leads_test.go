package amocrm

import (
	"testing"
	"time"

	"github.com/xelaj/go-dry"

	"github.com/stretchr/testify/assert"
)

var (
	somefield = CheckboxField(true)
	numfield  = NumericField(123)
)

func TestLead_MarshalMap(t *testing.T) {
	tests := []struct {
		name          string
		lead          *Lead
		want          map[string]string
		expectedError assert.ErrorAssertionFunc
	}{
		{
			lead: &Lead{
				ID:              8347634869347,
				Name:            "какое нибудь имя",
				ResponsibleUser: 24958295,
				CreatedBy:       6346834209,
				CreatedAt:       getTime("2020-11-07 19:52:16"),
				AccountID:       465436456,
				IsDeleted:       false,
				MainСontact:     445674567,
				GroupID:         445674567,
				Tags:            []string{"abcdefgh", "efgh"},
				Fields: map[string]Field{
					"somefield": &somefield,
					"numfield":  &numfield,
				},
			},
			want: map[string]string{
				"account_id":             "465436456",
				"custom_field:numfield":  "123",
				"name":                   "какое нибудь имя",
				"created_by":             "6346834209",
				"created_at":             "2020-11-07 19:52:16",
				"tags":                   "abcdefgh;efgh",
				"custom_field:somefield": "true",
				"responsible_user_id":    "24958295",
				"id":                     "8347634869347",
				"group_id":               "445674567",
			},
			expectedError: assert.NoError,
		},
	}
	for _, tcase := range tests {
		t.Run(tcase.name, func(t *testing.T) {
			got, err := tcase.lead.MarshalMap()
			if !tcase.expectedError(t, err) {
				return
			}

			assert.Equal(t, tcase.want, got)
		})
	}
}

func getTime(in string) time.Time {
	t, err := time.Parse(TimeFormatInSheets, in)
	dry.PanicIfErr(err)
	return t
}

func TestMergingLeads(t *testing.T) {
	type testcase struct {
		name    string
		reverse bool
		first   *Lead
		second  *Lead
		result  interface{}
		wantErr assert.ErrorAssertionFunc
	}

	cases := []testcase{
		{
			name: "amo -> table",
			first: &Lead{
				ID:   1488,
				Tags: nil,
				Fields: map[string]Field{
					"somefield": &abcdField,
					"foo":       &falseField,
					"bar":       &buzzField,
				},
			},
			second: &Lead{
				ID:   1488,
				Tags: []string{"a", "b"},
				Fields: map[string]Field{
					"somefield": &abcdField,
					"foo":       &trueField,
					"buzz":      &oofField,
				},
			},
			result: &Lead{
				ID:   1488,
				Tags: []string{"a", "b"},
				Fields: map[string]Field{
					"somefield": &abcdField,
					"foo":       &trueField,
					"bar":       &buzzField,
					"buzz":      &oofField,
				},
			},
		},
		{
			name:    "expect error",
			first:   nil,
			second:  &Lead{},
			result:  nil,
			wantErr: assert.Error,
		},
		{
			name:    "table -> amo",
			reverse: true,
			first: &Lead{
				Name:        "лучшая сделка",
				MainСontact: 1,
				ID:          1488,
				Fields: map[string]Field{
					"somefield": &abcdField,
					"foo":       &falseField,
				},
			},
			second: &Lead{
				Name:        "",
				MainСontact: 2,
				ID:          1488,
				Fields:      map[string]Field{},
			},
			result: &Lead{
				Name:        "",
				MainСontact: 2,
				ID:          1488,
				Fields:      map[string]Field{},
			},
		},
		{
			name:    "table -> amo 2",
			reverse: true,
			first: &Lead{
				Name: "",
				ID:   1488,
				Fields: map[string]Field{
					"Город": &SmartAddressField{AddressLine1: "Улица Пушкина, дом Колотушкина"},
				},
			},
			second: &Lead{
				Name: "123",
				ID:   1488,
				Fields: map[string]Field{
					"Город": &SmartAddressField{City: "Москва"},
				},
			},
			result: &Lead{
				Name: "123",
				ID:   1488,
				Fields: map[string]Field{
					"Город": &SmartAddressField{City: "Москва"},
				},
			},
		},
		{
			name:    "amo -> table 2",
			reverse: false,
			first: &Lead{
				ID:     1488,
				Fields: map[string]Field{},
			},
			second: &Lead{
				ID: 1488,
				Fields: map[string]Field{
					"Город": &buzzField,
				},
			},
			result: &Lead{
				ID: 1488,
				Fields: map[string]Field{
					"Город": &buzzField,
				},
			},
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			res, err := Merge(tt.first, tt.second, tt.reverse)
			if tt.wantErr == nil {
				tt.wantErr = assert.NoError
			}
			if !tt.wantErr(t, err) {
				return
			}
			assert.Equal(t, tt.result, res)
		})
	}
}
