package amocrm

import (
	"github.com/pkg/errors"
	"github.com/xelaj/errs"
	"github.com/xelaj/go-dry"
)

func (c *ClientSpecific) UnmarshalField(entityType, name, data string) (Field, error) {
	var fieldsEndpoint func() ([]*CustomFieldDescription, error)
	switch entityType {
	case "lead":
		fieldsEndpoint = c.GetLeadFields
	case "contact":
		fieldsEndpoint = c.GetContactFields
	default:
		return nil, errors.New("invalid entityType: " + entityType)
	}

	fields, err := fieldsEndpoint()
	if err != nil {
		return nil, errors.Wrap(err, "fetching field description data")
	}

	var selectedFieldDescription *CustomFieldDescription
	for _, item := range fields {
		if item.Name == name {
			selectedFieldDescription = item
			break
		}
	}
	if selectedFieldDescription == nil {
		return nil, errs.NotFound("custom field", name)
	}

	fieldType := GetCustomFieldType(selectedFieldDescription.Type)
	field, err := GenerateFieldObjectByType(fieldType)
	dry.PanicIfErr(err)

	err = field.UnmarshalText([]byte(data))
	if err != nil {
		return nil, errors.Wrap(err, "unmarshalling field")
	}

	return field, nil
}

//==========================================================================================================//

type EnumDescription struct {
	ID    int    `json:"id"`
	Value string `json:"value"`
	Sort  int    `json:"sort"`
}

type CustomFieldDescription struct {
	ID               int                `json:"id"`         // ID поля
	Name             string             `json:"name"`       // Название поля
	Type             string             `json:"type"`       // Тип поля
	AccountID        int                `json:"account_id"` //! что это???
	Code             string             `json:"code"`
	Sort             int                `json:"sort"` // Сортировка поля
	IsApiOnly        bool               `json:"is_api_only"`
	Enums            []*EnumDescription `json:"enums"`
	GroupID          interface{}        `json:"group_id"`          //! что это???
	RequiredStatuses interface{}        `json:"required_statuses"` //! что это???
	EntityType       string             `json:"entity_type"`       // тип объекта у которого есть это кастомное поле (leads, contacts etc.)
	Remind           interface{}        `json:"remind"`            //! что это???
	IsPredefined     bool               `json:"is_predefined"`     // Является ли поле предустановленным. Данный ключ возвращается только при получении списка полей контактов и компаний
	Settings         interface{}        `json:"settings"`          // Настройки поля. Данный ключ возвращается только при получении списка полей списков (каталогов)
}

type customFieldsResponse struct {
	TotalItems int         `json:"_total_items"`
	Page       int         `json:"_page"`
	PageCount  int         `json:"_page_count"`
	Links      interface{} `json:"_links"`
	Embedded   struct {
		Fields []*CustomFieldDescription `json:"custom_fields"`
	} `json:"_embedded"`
}

func (c *ClientSpecific) GetContactFields() ([]*CustomFieldDescription, error) {
	if v, ok := c.fieldsDescription["contacts"]; ok {
		return v, nil
	}

	v, err := c.getFields("contacts")
	if err != nil {
		return nil, err
	}

	c.fieldsDescription["contacts"] = v
	return v, nil
}

func (c *ClientSpecific) GetLeadFields() ([]*CustomFieldDescription, error) {
	if v, ok := c.fieldsDescription["leads"]; ok {
		return v, nil
	}

	v, err := c.getFields("leads")
	if err != nil {
		return nil, err
	}

	c.fieldsDescription["leads"] = v
	return v, nil
}

func (c *ClientSpecific) getFields(of string) ([]*CustomFieldDescription, error) {
	// create response
	response := new(customFieldsResponse)
	err := c.Request("GET", "/"+of+"/custom_fields", nil, response, nil)
	if err != nil {
		return nil, err
	}

	return response.Embedded.Fields, nil
}
