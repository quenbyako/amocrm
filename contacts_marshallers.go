package amocrm

import (
	"encoding/json"
	"strings"
	"time"

	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"github.com/xelaj/go-dry"
)

//==========================================================================================================//
//                                               CSV Parsing                                                //
//==========================================================================================================//

func (c *Contact) MarshalMap() (map[string]string, error) {
	fields := map[string]interface{}{
		"id":              c.ID,
		"name":            c.Name,
		"first_name":      c.FirstName,
		"last_name":       c.LastName,
		"created_by":      c.CreatedBy,
		"created_at":      c.CreatedAt,
		"updated_at":      c.UpdatedAt,
		"updated_by":      c.UpdatedBy,
		"companies":       c.Companies,
		"account_id":      c.AccountID,
		"group_id":        c.GroupID,
		"leads":           c.Leads,
		"closest_task_at": c.ClosestTaskAt,
		"tags":            c.Tags,
		"customers":       c.Customers,
	}
	for k, field := range c.Fields {
		fields[customFieldCode+":"+k] = field
	}

	res := make(map[string]string)
	for key, elem := range fields {
		if dry.ReflectIsInteger(elem) {
			// если это нулевое число, пропускаем
			if dry.ConvertInt(elem) == 0 {
				continue
			}
		}
		if t, ok := elem.(time.Time); ok {
			// если это нулевое время, пропускаем
			if t.IsZero() {
				continue
			}
		}

		converted, err := MarshalText(elem)
		if err != nil {
			return nil, errors.Wrap(err, key)
		}
		if converted != "" {
			res[key] = converted
		}
	}
	return res, nil
}

func (c *Contact) UnmarshalMap(in map[string]string) error {
	if c.client == nil {
		return errors.New("can't unmarshall contact, it's client set to nil")
	}

	var err error
	m := make(map[string]interface{})
	for k, v := range in {
		// поскольку mapstructure умеет преобразовывать типы неявно, можем смело перекопировать
		// мап из строк в интерфейс, нам останется только обработать те значения, которые не может
		// нормально обработать mapstructure
		v = strings.Trim(v, " \t\r\n")
		m[k] = v

		// зачем нам тут пустые значения?
		if v == "" {
			delete(m, k)
		}
	}
	delete(m, "") // для гарантии

	if val, ok := in["companies"]; ok {
		m["companies"], err = SplitStringInts(val)
		if err != nil {
			return errors.Wrap(err, "UnmarshalMap at parsing companies")
		}
	}
	if val, ok := in["leads"]; ok {
		m["leads"], err = SplitStringInts(val)
		if err != nil {
			return errors.Wrap(err, "UnmarshalMap at parsing leads")
		}
	}

	if val, ok := in["tags"]; ok {
		m["tags"] = StringToList(val)
	}

	if val, ok := in["customers"]; ok {
		m["customers"], err = SplitStringInts(val)
		if err != nil {
			return errors.Wrap(err, "UnmarshalMap at parsing customers")
		}
	}

	// custom fields
	customFields := make(map[string]Field)
	for k, v := range in {
		if !strings.HasPrefix(k, customFieldCode+":") || v == "" {
			continue
		}

		customFieldName := strings.TrimPrefix(k, customFieldCode+":")

		field, err := c.client.UnmarshalField("contact", customFieldName, v)
		if err != nil {
			return errors.Wrap(err, "parsing '"+k+"'")
		}

		customFields[customFieldName] = field

		delete(m, k)
	}
	m[customFieldCode] = customFields

	decoder, _ := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		DecodeHook:       StringToTimeHookFunc(TimeFormatInSheets, StringToSlice(nil)),
		ErrorUnused:      true,
		WeaklyTypedInput: true,
		Result:           c,
		TagName:          "csv",
	})
	err = decoder.Decode(m)
	if err != nil {
		return err
	}

	return nil
}

//==========================================================================================================//
//                                               JSON Parsing                                               //
//==========================================================================================================//

type contactJSON struct {
	ID              int             `json:"id"`
	Name            string          `json:"name"`
	FirstName       string          `json:"first_name"`
	LastName        string          `json:"last_name"`
	ResponsibleUser int             `json:"responsible_user_id"`
	GroupID         int             `json:"group_id"`
	CreatedBy       int             `json:"created_by"`
	UpdatedBy       int             `json:"updated_by"`
	CreatedAt       *int64          `json:"created_at"`
	UpdatedAt       *int64          `json:"updated_at"`
	ClosestTaskAt   *int64          `json:"closest_task_at"`
	Fields          json.RawMessage `json:"custom_fields_values"`
	AccountID       int             `json:"account_id"`
	Embedded        struct {
		Tags []struct {
			ID   int    `json:"id"`
			Name string `json:"name"`
		} `json:"tags"`
		Companies []struct {
			ID int `json:"id"`
		} `json:"companies"`
		Customers []struct {
			ID int `json:"id"`
		} `json:"customers"`
		Leads []struct {
			ID int `json:"id"`
		} `json:"leads"`
		CatalogElements []struct {
			ID        int         `json:"id"`
			Metadata  interface{} `json:"metadata"`
			Quantity  int         `json:"quantity"`
			CatalogID int         `json:"catalog_id"`
		} `json:"catalog_elements"`
	} `json:"_embedded"`
}

func (c *Contact) UnmarshalJSON(b []byte) error {
	m := new(contactJSON)
	err := json.Unmarshal(b, m)

	c.ID = m.ID
	c.Name = m.Name
	c.FirstName = m.FirstName
	c.LastName = m.LastName
	c.ResponsibleUser = m.ResponsibleUser
	c.GroupID = m.GroupID
	c.CreatedBy = m.CreatedBy
	c.UpdatedBy = m.UpdatedBy
	if m.CreatedAt != nil {
		c.CreatedAt = time.Unix(*m.CreatedAt, 0)
	}
	if m.UpdatedAt != nil {
		c.UpdatedAt = time.Unix(*m.UpdatedAt, 0)
	}
	if m.ClosestTaskAt != nil {
		c.ClosestTaskAt = time.Unix(*m.ClosestTaskAt, 0)
	}

	if len(m.Fields) > 0 {
		c.Fields, err = unmarshalCustomFields(m.Fields)
		if err != nil {
			return errors.Wrap(err, "parsing custom fields")
		}
	}
	c.AccountID = m.AccountID
	if len(m.Embedded.Companies) != 0 {
		companies := make([]int, len(m.Embedded.Companies))
		for i, item := range m.Embedded.Companies {
			companies[i] = item.ID
		}
		c.Companies = companies
	}
	if len(m.Embedded.Leads) != 0 {
		leads := make([]int, len(m.Embedded.Leads))
		for i, item := range m.Embedded.Leads {
			leads[i] = item.ID
		}
		c.Leads = leads
	}
	if len(m.Embedded.Tags) != 0 {
		tags := make([]string, len(m.Embedded.Tags))
		for i, item := range m.Embedded.Tags {
			tags[i] = item.Name
		}
		c.Tags = tags
	}
	if len(m.Embedded.Leads) != 0 {
		customers := make([]int, len(m.Embedded.Customers))
		for i, item := range m.Embedded.Customers {
			customers[i] = item.ID
		}
		c.Customers = customers
	}

	return err
}

/*
func (e *Contact) MarshalJSON() ([]byte, error) {
	m := make(map[string]interface{})
	m["id"] = e.ID

	m["name"] = e.Name
	if e.FirstName != "" {
		m["first_name"] = e.FirstName
	}
	if e.LastName != "" {
		m["last_name"] = e.LastName
	}

	m["responsible_user_id"] = e.ResponsibleUser
	m["created_by"] = e.CreatedBy
	m["created_at"] = e.CreatedAt.Unix()
	m["updated_at"] = e.UpdatedAt.Unix()
	m["account_id"] = e.AccountID
	m["updated_by"] = e.UpdatedBy
	m["group_id"] = e.GroupID
	//m["company"] = map[string]interface{}{
	//	"id":   e.Companies.ID,
	//	"name": e.Company.Name,
	//}

	l := e.Leads
	if e.Leads != nil {
		l = make([]int, 0)
	}
	m["leads"] = map[string]interface{}{
		"id": l,
	}

	m["closest_task_at"] = e.ClosestTaskAt.Unix()
	m["tags"] = e.Tags

	type field struct {
		Name   string `json:"name"`
		Values []struct {
			Value string `json:"value"`
		} `json:"values"`
	}

	fields := make([]field, 0)
	for name, values := range e.Fields {
		v := make([]struct {
			Value string `json:"value"`
		}, len(values))
		for i, val := range values {
			v[i] = struct {
				Value string `json:"value"`
			}{val}
		}

		fields = append(fields, field{
			Name:   name,
			Values: v,
		})
	}
	m[customFieldCode] = fields
	m["customers"] = e.Customers

	return json.Marshal(&m)
}
*/
