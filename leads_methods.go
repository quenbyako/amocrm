package amocrm

import (
	"encoding/json"
	"fmt"
	"log"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
)

type WithParameter string

const (
	// leads
	WithCatalogElements        WithParameter = "catalog_elements"           // Добавляет в ответ связанные со сделками элементы списков
	WithIsPriceModifiedByRobot WithParameter = "is_price_modified_by_robot" // Добавляет в ответ свойство, показывающее, изменен ли в последний раз бюджет сделки роботом
	WithLossReason             WithParameter = "loss_reason"                // Добавляет в ответ расширенную информацию по причине отказа
	WithContacts               WithParameter = "contacts"                   // Добавляет в ответ информацию о связанных со сделкой контактах
	WithOnlyDeleted            WithParameter = "only_deleted"               // Если передать данный параметр, то в ответе на запрос метода, вернутся удаленные сделки, которые еще находятся в корзине. В ответ вы получите модель сделки, у которой доступны дату изменения, ID пользователя сделавшего последнее изменение, её ID и параметр is_deleted = true.
	WithSourceID               WithParameter = "source_id"                  // Добавляет в ответ ID источника

	// contacts
	//WithCatalogElements WithParameter = "catalog_elements" // Добавляет в ответ связанные с контактами элементы списков
	WithLeads     WithParameter = "leads"     // Добавляет в ответ связанные с контактами сделки
	WithCustomers WithParameter = "customers" // Добавляет в ответ связанных с контактами покупателей
)

func (w WithParameter) String() string {
	return string(w)
}

type GetLeadsParams struct {
	With        []WithParameter
	DeletedOnly bool
	Limit       int
	Page        int
	Query       string
	Filter      string // https://www.amocrm.ru/developers/content/crm_platform/filters-api
	Order       string // paramName:asc (created_at|updated_at|id):(asc|desc)
}

func (p *GetLeadsParams) toValues() url.Values {
	v := url.Values{}

	if p.DeletedOnly {
		if p.With != nil {
			p.With = make([]WithParameter, 0)
		}
		p.With = append(p.With, WithOnlyDeleted)
	}
	if len(p.With) != 0 {
		elements := make([]fmt.Stringer, len(p.With))
		for i, item := range p.With {
			elements[i] = fmt.Stringer(item)
		}
		v.Set("with", stringifyWithParams(elements...))
	}
	if p.Limit != 0 {
		v.Set("limit", strconv.Itoa(p.Limit))
	}
	if p.Page != 0 {
		v.Set("page", strconv.Itoa(p.Page))
	}
	if p.Query != "" {
		v.Set("query", p.Query)
	}
	if p.Filter != "" {
		v.Set("filter", p.Filter)
	}
	if p.Order != "" {
		v.Set("order", p.Order)
	}
	return v
}

type leadsResponse struct {
	Embedded struct {
		Leads []*Lead `json:"leads"`
	} `json:"_embedded"`
	// _page
	// _links
}

func (c *ClientSpecific) GetLeads(params GetLeadsParams) ([]*Lead, error) {
	resp := new(leadsResponse)
	err := c.Request("GET", "/leads", params.toValues(), resp, nil)
	if err != nil {
		return nil, err
	}

	for _, item := range resp.Embedded.Leads {
		item.client = c
	}

	return resp.Embedded.Leads, nil
}

type getLeadsByID struct {
	With []WithParameter
}

func (p getLeadsByID) toValues() url.Values {
	v := make(url.Values)
	if len(p.With) != 0 {
		elements := make([]fmt.Stringer, len(p.With))
		for i, item := range p.With {
			elements[i] = fmt.Stringer(item)
		}
		v.Set("with", stringifyWithParams(elements...))
	}

	return v
}

func (c *ClientSpecific) GetLeadByID(id int, with ...WithParameter) (*Lead, error) {
	response := new(Lead)
	err := c.Request("GET", "/leads/"+strconv.Itoa(id), getLeadsByID{With: with}.toValues(), response, nil)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (c *ClientSpecific) GetAllLeads(limit int, with []WithParameter) ([]*Lead, error) {
	leadsAll := make([]*Lead, 0)

	for page := 0; ; page++ {
		if debugMode {
			print(".")
		}
		leads, err := c.GetLeads(GetLeadsParams{
			With:  with,
			Page:  page,
			Limit: ContactsMaxLimitPerRequest,
		})
		if err != nil {
			// если нет контента, то отдаем все что нашли, дальше то точно контента не будет
			if _, ok := err.(*ErrorNoContent); ok {
				return leadsAll, nil
			}
			return nil, err
		}

		leadsAll = append(leadsAll, leads...)

		if limit > 0 && len(leadsAll) >= limit {
			return leadsAll[:limit], nil
		}

		if len(leads) < ContactsMaxLimitPerRequest {
			break // если он вернул меньше, то последующие зарпросы 100% ничего не вернут
		}
	}

	return leadsAll, nil
}

type updateLeadParams struct {
	Name               string          `json:"name,omitempty"`                 // Название сделки. Поле не является обязательным
	Price              int             `json:"price,omitempty"`                // Бюджет сделки. Поле не является обязательным
	StatusID           int             `json:"status_id,omitempty"`            // ID статуса, в который добавляется сделка. Поле не является обязательным, по-умолчанию – первый этап главной воронки
	PipelineID         int             `json:"pipeline_id,omitempty"`          // ID воронки, в которую добавляется сделка. Поле не является обязательным
	CreatedBy          int             `json:"created_by,omitempty"`           // ID пользователя, создающий сделку. При передаче значения 0, сделка будет считаться созданной роботом. Поле не является обязательным
	UpdatedBy          int             `json:"updated_by,omitempty"`           // ID пользователя, изменяющий сделку. При передаче значения 0, сделка будет считаться измененной роботом. Поле не является обязательным
	ClosedAt           int64           `json:"closed_at,omitempty"`            // Дата закрытия сделки, передается в Unix Timestamp. Поле не является обязательным
	CreatedAt          int64           `json:"created_at,omitempty"`           // Дата создания сделки, передается в Unix Timestamp. Поле не является обязательным
	UpdatedAt          int64           `json:"updated_at,omitempty"`           // Дата изменения сделки, передается в Unix Timestamp. Поле не является обязательным
	LossReasonID       int             `json:"loss_reason_id,omitempty"`       // ID причины отказа. Поле не является обязательным
	ResponsibleUser    int             `json:"responsible_user_id,omitempty"`  // ID пользователя, ответственного за сделку. Поле не является обязательным
	CustomFieldsValues json.RawMessage `json:"custom_fields_values,omitempty"` // Массив, содержащий информацию по дополнительным полям, заданным для данной сделки. Поле не является обязательным. Примеры заполнения полей
	// _embedded 	object 	// Данные вложенных сущностей, при создании и редактировании можно передать только теги. Поле не является обязательным
	// _embedded[tags] 	array|null 	// Данные тегов, добавляемых к сделке
	// _embedded[tags][0] 	object 	// Модель тега, добавляемого к сделке. Необходимо указать id или name
	// _embedded[tags][0][id] 	int 	// ID тега
	// _embedded[tags][0][name] 	string 	// Название тега
}

func (c *ClientSpecific) UpdateLead(lead *Lead) error {
	if lead.ID == 0 {
		return errors.New("лид необходимо создать прежде чем обновлять (lead_id == 0)")
	}

	if debugMode {
		if !strings.Contains(strings.ToLower(lead.Name), "тест") {
			log.Println("ой, другие лиды пока нельзя менять ради безопасности")
			return errors.New("lead is restricted to edit")
		}
	}

	existed, err := c.GetLeadByID(lead.ID)
	if err != nil {
		return errors.Wrap(err, "лид не найден в сервисе")
	}

	var requireToSend bool // если хоть одно поле изменилось, то мы отсылаем апдейт, иначе не пересылаем лишних
	//                        реквестов.
	body := updateLeadParams{
		UpdatedAt: time.Now().Unix(),
	}

	if lead.Name != existed.Name {
		body.Name = lead.Name
		requireToSend = true
	}
	if lead.Price != existed.Price {
		body.Price = lead.Price
		requireToSend = true
	}
	if lead.StatusID != existed.StatusID {
		body.StatusID = lead.StatusID
		requireToSend = true
	}
	if lead.Pipeline != existed.Pipeline {
		body.PipelineID = lead.Pipeline
		requireToSend = true
	}
	if lead.CreatedBy != existed.CreatedBy {
		body.CreatedBy = lead.CreatedBy
		requireToSend = true
	}
	if lead.UpdatedBy != existed.UpdatedBy {
		body.UpdatedBy = lead.UpdatedBy
		requireToSend = true
	}
	// unix потому что нельзя учитывать наноескунды
	if lead.ClosedAt.Unix() != existed.ClosedAt.Unix() {
		body.ClosedAt = lead.ClosedAt.Unix()
		requireToSend = true
	}
	// unix потому что нельзя учитывать наноескунды
	if lead.CreatedAt.Unix() != existed.CreatedAt.Unix() {
		body.ClosedAt = lead.ClosedAt.Unix()
		requireToSend = true
	}

	updatedCustomFields := UpdateFields(lead.Fields, existed.Fields)
	if len(updatedCustomFields) > 0 {
		var err error
		body.CustomFieldsValues, err = c.marshalFieldsForUpdates("lead", updatedCustomFields)
		if err != nil {
			return errors.Wrap(err, "marshalling fields")
		}
		requireToSend = true
	}

	if !requireToSend {
		return nil
	}

	data, _ := json.Marshal(body)
	response := new(Lead)
	err = c.Request("PATCH", "/leads/"+strconv.Itoa(lead.ID), nil, response, data)
	if err != nil {
		return err
	}

	return nil
}
