package amocrm

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	f1text1 = TextField("any random text")
	f2text1 = TextField("any edited text")

	f2text2 = TextField("")
)

func TestUpdateFields(t *testing.T) {
	tests := []struct {
		f1   map[string]Field
		f2   map[string]Field
		want map[string]Field
	}{
		{
			f1: map[string]Field{
				"text": &f1text1,
			},
			f2: map[string]Field{
				"text": &f2text1,
			},
			want: map[string]Field{
				"text": &f2text1,
			},
		},
		{
			f1: map[string]Field{
				"text": &f1text1,
			},
			f2: map[string]Field{
				"text": &f1text1,
			},
			want: map[string]Field{},
		},
		{
			f1: map[string]Field{
				"text": &f1text1,
			},
			f2: map[string]Field{},
			want: map[string]Field{
				"text": &f2text2,
			},
		},
	}
	for _, tt := range tests {
		t.Run("", func(t *testing.T) {
			assert.Equal(t, tt.want, UpdateFields(tt.f1, tt.f2))
		})
	}
}
