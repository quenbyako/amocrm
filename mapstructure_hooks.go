package amocrm

import (
	"bytes"
	"encoding/csv"
	"reflect"
	"time"

	"github.com/mitchellh/mapstructure"
	"github.com/xelaj/go-dry"
)

func StringToTimeHookFunc(layout string, next mapstructure.DecodeHookFuncType) mapstructure.DecodeHookFuncType {
	return func(
		f reflect.Type,
		t reflect.Type,
		data interface{}) (interface{}, error) {
		if f.Kind() != reflect.String || t != reflect.TypeOf(time.Time{}) {
			if next != nil {
				return next(f, t, data)
			}
			return data, nil
		}

		if data.(string) == "" {
			return time.Time{}, nil
		}

		return time.Parse(layout, data.(string))
	}
}

func StringToSlice(next mapstructure.DecodeHookFuncType) mapstructure.DecodeHookFuncType {
	return func(
		f reflect.Type,
		t reflect.Type,
		data interface{}) (interface{}, error) {
		if f.Kind() != reflect.String {
			if next != nil {
				return next(f, t, data)
			}
			return data, nil
		}
		if t != reflect.TypeOf([]string{}) {
			if next != nil {
				return next(f, t, data)
			}
			return data, nil
		}

		if data.(string) == "" {
			return []string{}, nil
		}

		splitted, err := csv.NewReader(bytes.NewBufferString(data.(string))).Read()
		dry.PanicIfErr(err)

		return splitted, nil
	}
}