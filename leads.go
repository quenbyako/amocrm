package amocrm

import (
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
)

type Lead struct {
	ID              int              `csv:"id"`                            // Уникальный идентификатор сделки
	Name            string           `csv:"name"`                          // Название сделки
	Price           int              `csv:"price"`                         // Бюджет сделки
	ResponsibleUser int              `csv:"responsible_user_id,omitempty"` // id ответственного пользователя
	CreatedBy       int              `csv:"created_by,omitempty"`          // id пользователя создавшего сделку
	CreatedAt       time.Time        `csv:"created_at,omitempty"`          // Время и дата создания сделки
	UpdatedBy       int              `csv:"updated_by,omitempty"`          //
	UpdatedAt       time.Time        `csv:"updated_at,omitempty"`          // Время и дата изменения сделки
	AccountID       int              `csv:"account_id,omitempty"`          // id аккаунта на котором создана сделка
	IsDeleted       bool             `csv:"is_deleted,omitempty"`          // Удалена сделка или нет. Удалённые сделки могут находиться в “удалённых”.
	MainСontact     int              `csv:"main_contact,omitempty"`        // Массив содержащий информацию о главном контакте сделки
	GroupID         int              `csv:"group_id,omitempty"`            // id группы в которой состоит пользователь ответственный за данную сделку
	ClosedAt        time.Time        `csv:"closed_at,omitempty"`           // Время и дата, когда была завершена данная сделка
	ClosestTaskAt   time.Time        `csv:"closest_task_at,omitempty"`     // Время ближайшей задачи по данной сделки
	Tags            []string         `csv:"tags,omitempty"`                // Массив содержащий информацию по тегам, прикреплённым к данной сделке
	Fields          map[string]Field `csv:"custom_field,omitempty"`        // Массив содержащий информацию по дополнительным полям, заданным для данной сделки
	Contacts        []int            `csv:"contacts,omitempty"`            // Массив содержащий информацию по контактам прикреплённым к данной сделке
	StatusID        int              `csv:"status_id,omitempty"`           // id этапа цифровой воронки, на котором находится данная сделка
	Sale            int              `csv:"sale,omitempty"`                // Бюджет сделки
	Pipeline        int              `csv:"pipeline_id,omitempty"`         // id цифровой воронки, в которой находится данная сделка
	client          *ClientSpecific  `csv:"-" pp:"noprint"`                //
}

func (l *Lead) AmoID() int {
	return l.ID
}

func (l *Lead) GetName() string {
	return l.Name
}

func (l *Lead) Type() string {
	return "lead"
}

/*
func companyName(c *Company) string {
	if c == nil {
		return ""
	}
	return c.String()
}
*/

func (l *Lead) ResetClient(client *ClientSpecific) {
	l.client = client
}

func (l *Lead) GetID() int {
	return l.ID
}

// amo, table, amo -> table
func (l1 *Lead) MergeToTable(l2 *Lead) (*Lead, error) {
	return l1.merge(l2, false)
}

// table, amo, table -> amo
func (l1 *Lead) MergeToAmo(l2 *Lead) (*Lead, error) {
	return l1.merge(l2, true)
}

func (l1 *Lead) merge(l2 *Lead, clearIfBHasZeroValue bool) (*Lead, error) {
	if l1.client != l2.client {
		return nil, errors.New("leads has different clients")
	}
	if l1.ID != l2.ID {
		return nil, errors.New("leads has different amo ids")
	}

	merged, err := Merge(l1, l2, clearIfBHasZeroValue)
	if err != nil {
		return nil, errors.Wrapf(err, "merging lead %v", l1.ID)
	}
	res := merged.(*Lead)
	res.client = l1.client
	return res, nil
}

func (l *Lead) Hash() string {
	prev := strings.Join(SprintfAllItems(
		l.ID,
		l.ResponsibleUser,
		l.CreatedBy,
		l.CreatedAt.Unix(),
		l.UpdatedBy,
		l.UpdatedAt.Unix(),
		l.AccountID,
		l.IsDeleted,
		l.MainСontact,
		l.GroupID,
		l.ClosedAt.Unix(),
		l.ClosestTaskAt.Unix(),
		sortStrings(l.Tags),
		kvFromMap(l.Fields).String(),
		l.StatusID,
		l.Pipeline,
	), ";")

	return prev
	// hash := md5.Sum([]byte(prev))
	// return hex.EncodeToString(hash[:])
}

func timeUnix(t *time.Time) string {
	// то ли я словил баг, то ли не так делаю
	// иногда time.Unix() отдает -62135596800, это 1 января 1 года 00:00, если даты нет
	// ВОЗМОЖНО мне стоило бы использовать указатель на время, но уже поздно.
	val := int(t.UTC().Unix())
	if val == -62135596800 {
		return "0"
	}
	return strconv.Itoa(val)
}

func (l *Lead) FilterCustomFields(f func(string) bool) {
	for k := range l.Fields {
		if !f(k) {
			delete(l.Fields, k)
		}
	}
}

func (l *Lead) GetCustomFields() map[string]Field {
	return l.Fields
}
