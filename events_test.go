package amocrm

import (
	"context"
	"testing"
	"time"

	"github.com/k0kubun/pp"
)

func TestGettingEvents(t *testing.T) {
	pp.Println(client.GetLastEvents(GetLastEventsParams{}))

}

func TestEventListener(t *testing.T) {
	listener := client.NewEventListener()

	listener.On("lead_changed", onContactChanged)

	ctx, cancel := context.WithCancel(context.Background())
	err := listener.Start(ctx)
	go func() {
		for {
			pp.Println(<-err)
		}
	}()

	end := make(chan struct{})
	time.AfterFunc(time.Second*5, func() {
		cancel()
		end <- struct{}{}
	})
	<-end
}

func onContactChanged(e *Event) error {
	pp.Println("processing", e)
	return nil
}
