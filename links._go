package amocrm

import (
	"net/url"
	"path/filepath"
	"regexp"
	"strconv"

	"github.com/pkg/errors"
)

func (c *ClientBase) LeadLink(id int) string {
	return c.url.String() + "/leads/detail/" + strconv.Itoa(id)
}

func (c *ClientBase) ContactLink(id int) string {
	return c.url.String() + "/contacts/detail/" + strconv.Itoa(id)
}

func (c *ClientBase) ParseLink(link string) (string, string, error) {
	l, err := url.Parse(link)
	if err != nil {
		return "", "", err
	}

	switch {
	case regexp.MustCompile("/leads/detail/[0-9]+").MatchString(l.Path):
		_, leadID := filepath.Split(l.Path)
		return "lead", leadID, nil
	case regexp.MustCompile("/contacts/detail/[0-9]+").MatchString(l.Path):
		_, contactID := filepath.Split(l.Path)
		return "contact", contactID, nil

	default:
		return "", "", errors.New("unknown link type")
	}
}
