package amocrm

import (
	"net/url"
	"strings"
)

type AccountWithParam string

const (
	WithAmojoID          AccountWithParam = "amojo_id"
	WithAmojoRights      AccountWithParam = "amojo_rights"
	WithUsersGroups      AccountWithParam = "users_groups"
	WithTaskTypes        AccountWithParam = "task_types"
	WithVersion          AccountWithParam = "version"
	WithEntityNames      AccountWithParam = "entity_names"
	WithDatetimeSettings AccountWithParam = "datetime_settings"
)

func (c *ClientSpecific) GetAccount(with ...AccountWithParam) (*Account, error) {
	stringedWith := make([]string, len(with))
	for i, item := range with {
		stringedWith[i] = string(item)
	}
	withStr := strings.Join(stringedWith, ",")

	// set values
	values := url.Values{}
	if len(with) != 0 {
		values["with"] = []string{withStr}
	}

	// create response
	response := new(Account)
	err := c.Request("GET", "/account", values, response, nil)
	if err != nil {
		return nil, err
	}

	return response, nil
}
